#-*- coding: utf-8 -*-

import cx_Oracle
import DTOStatistique
import datetime

class DAOOracleStatistique():
    __shared_state = {} #Borg/Singleton design pattern
    def __init__(self):
        self.__dict__ = self.__shared_state #Borg/Singleton design pattern
        self.connectionBD()

    def connectionBD(self):
        #Création d'une connection à la BD
        try:
            self.connection = cx_Oracle.connect("e0957119","Baba","10.57.4.60/DECINFO.edu")
            #Création du curseur
            self.cur = self.connection.cursor()
        except :
            return 1

    def creerPartie(self,dto):
        query="SELECT id from niveau WHERE nomniveau ='%s'" %(dto.getNomNiveau())
        self.cur.execute(query)
        result=self.cur.fetchone()
        idniv=result[0]
        self.datePartieCourrante=datetime.datetime.strftime(datetime.datetime.now(), '%Y/%m/%d %H:%M:%S')
        query="INSERT INTO partie (id_niveau,date_partie,abandon1,abandon2) VALUES ('%i',TO_DATE('%s','yyyy/mm/dd hh24:mi:ss'),'%i','%i')" %(idniv,self.datePartieCourrante,dto.getAbandon1(),dto.getAbandon2())
        self.cur.execute(query)
        self.connection.commit()

    def setResultatPartie(self,dto):
        #On va chercher le id de la partie jouée
        query="SELECT id from partie WHERE date_partie = TO_DATE('%s','yyyy/mm/dd hh24:mi:ss')" %(self.datePartieCourrante)
        self.cur.execute(query)
        result=self.cur.fetchone()
        idPartie=result[0]

        #On update la table partie pour y inscrire le gagnant, le perdant et enlever la mention abandon au joueurs
        query="UPDATE partie SET id_gagnant='%i',id_perdant='%i',abandon1=NULL, abandon2=NULL WHERE id='%i'" %(dto.getGagnant(),dto.getPerdant(),idPartie)
        self.cur.execute(query)

        #On inscrit dans la base de donnée les statistiques  pour chacune des cases
        for i in dto.getStatsCase():
            query="INSERT INTO partie_case (id_partie,x,y,id_joueur,dommage_recu,dommage_infliger,temps) VALUES ('%i','%i','%i','%i','%f','%f','%f')" %(idPartie,i[0],i[1],i[2],i[3],i[4],i[5])
            self.cur.execute(query)  

        #On inscrit dans la base de donnée les statistique pour chacune des armes
        for key, value in dto.getStatsArmePartie().iteritems():
            query="SELECT id from arme WHERE typeArme = '%s'" %(key)
            self.cur.execute(query)
            result=self.cur.fetchone()
            idArme=result[0]

            query="INSERT INTO joueur_arme_partie (id_joueur,id_arme,id_partie,tir,dommage) VALUES ('%i','%i','%i','%i','%f')" %(dto.getGagnant(),idArme,idPartie,value[0],value[1])
            self.cur.execute(query)
            query="INSERT INTO joueur_arme_partie (id_joueur,id_arme,id_partie,tir,dommage) VALUES ('%i','%i','%i','%i','%f')" %(dto.getPerdant(),idArme,idPartie,value[2],value[3])
            self.cur.execute(query)
            self.connection.commit()

    def getNombrePartie(self):
        query="SELECT COUNT(*) FROM partie"
        self.cur.execute(query)
        result=self.cur.fetchone()
        return result[0]

    def getNombrePartieDerniereHeure(self):
        query="SELECT COUNT(*) FROM partie WHERE date_partie > SYSDATE - (60/1440)"
        self.cur.execute(query)
        result=self.cur.fetchone()
        return result[0]

    def getStatistiquePartie(self,idPartie):
        dto=DTOStatistique.DTOStatistique()
        query="SELECT * FROM partie WHERE id='%i'" %(idPartie)
        self.cur.execute(query)
        result=self.cur.fetchone()
        query="SELECT nomniveau FROM niveau WHERE id='%i'" %(result[1])
        self.cur.execute(query)
        temp=self.cur.fetchone()
        nomNiv=temp[0]

        dto.setNomNiveau(nomNiv)
        dto.setDatePartie(result[2])
        dto.setGagnant(result[3])
        dto.setPerdant(result[4])
        dto.setAbandon1(result[5])
        dto.setAbandon2(result[6])

        query="SELECT x,y,id_joueur,dommage_recu,dommage_infliger,temps FROM partie_case WHERE id_partie='%i'" %(idPartie)
        self.cur.execute(query)
        result=self.cur.fetchall()
        statscase=[]
        for i in result:
            temp=[]
            for j in i:
                temp.append(j)
            statscase.append(temp)
        dto.setStatsCase(statscase)

        return dto

    def getStatistiquePagePublique(self,nomJoueur):
        dto=DTOStatistique.DTOStatistique()
        query="SELECT id FROM utilisateur WHERE nomjoueur = '%s'" %(nomJoueur)
        self.cur.execute(query)
        result=self.cur.fetchone()
        idJoueur=result[0]

        #Nombre de victoire
        query="SELECT COUNT(*) FROM partie WHERE id_gagnant='%i'" %(idJoueur)
        self.cur.execute(query)
        result=self.cur.fetchone()
        dto.setNbrVictoire(result[0])

        #Nombre de défaite
        query="SELECT COUNT(*) FROM partie WHERE id_perdant='%i'" %(idJoueur)
        self.cur.execute(query)
        result=self.cur.fetchone()
        dto.setNbrDefaite(result[0])

        #Nombre d'abandon
        query="SELECT COUNT(*) FROM partie WHERE abandon1='%i' OR abandon2='%i'" %(idJoueur,idJoueur)
        self.cur.execute(query)
        result=self.cur.fetchone()
        dto.setNbrAbandon(result[0])

        #Nombre de niveaux créé
        query="SELECT COUNT(*) FROM niveau WHERE id_utilisateur='%i'" %(idJoueur)  #IS IT ID JOUEUR?????? TO CHECK
        self.cur.execute(query)
        result=self.cur.fetchone()
        dto.setNbrNiveauxCree(result[0])

        #Stats des armes du joueur
        typeArme=["Grenade","Mitraillette","Piege","Shotgun","Guide","Spring"]
        dictArme={}
        for i in typeArme:
            temp=[]
            query="SELECT id FROM arme WHERE typeArme='%s'" %(i)
            self.cur.execute(query)
            result=self.cur.fetchone()
            idArme=result[0]

            query="SELECT SUM(tir) FROM joueur_arme_partie WHERE id_joueur='%i' AND id_arme='%i'" %(idJoueur,idArme)
            self.cur.execute(query)
            result=self.cur.fetchone()
            temp.append(result[0])

            query="SELECT SUM(dommage) FROM joueur_arme_partie WHERE id_joueur='%i' AND id_arme='%i'" %(idJoueur,idArme)
            self.cur.execute(query)
            result=self.cur.fetchone()
            temp.append(result[0])

            dictArme[i]=temp

        dto.setStatsArmeJoueur(dictArme)
        return dto

    def getIdJoueur(self, nom):
        query="SELECT id FROM utilisateur WHERE nomjoueur='%s'" %(nom)
        self.cur.execute(query)
        result=self.cur.fetchone()
        return result[0]


