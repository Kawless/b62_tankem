#-*- coding: utf-8 -*-

import cx_Oracle
import dtoUser
#import bcrypt #UNCOMMENT FOR BCRYPT

class DAOOracleUser():

    def connectionBD(self):
        #Création d'une connection à la BD
        try:
            self.connection = cx_Oracle.connect("e0957119","Baba","10.57.4.60/DECINFO.edu")
        except :
            return 1

        #Création du curseur
        self.cur = self.connection.cursor()

    def fermerBD(self):
        self.cur.close()
        self.connection.close()

    def getUser(self, username): #Va chercher toute les informations d'un usager
        abort=0
        if (self.connectionBD()):
            print("Impossible de se connecter à la base de donnée")
            return 0
        else:
            try:
                query = "SELECT * from utilisateur WHERE nomJoueur = '%s'" %(username)
                self.cur.execute(query)
                result=self.cur.fetchone()
            except:
                abort=1
                result=0
            if result==[] or result==None:
                abort=1
                print("L'usager n'existe pas.")
            if(abort!=1):
                dto=dtoUser.DTOUser() #CREER UN DTO ET Y METTRE LES PARAMETRE DE BASE
                dto.setAll(result[1],result[2],result[3],result[4],result[5],result[6],result[7],result[8],result[9],None,[],[])
                currentID=result[0]

                #VA CHERCHER LES NIVEAUX CREER
                liste=[]
                query="SELECT id_niveau from niveaux_crees WHERE id_utilisateur = '%i'" %(currentID)
                self.cur.execute(query)
                result=self.cur.fetchall()
                for i in result:
                    query="SELECT nomniveau from niveau WHERE id = '%i'" %(i[0])
                    self.cur.execute(query)
                    temp=self.cur.fetchone()
                    liste.append(temp[0])
                dto.setListeNiveauxCrees(liste)

                #VA CHERCHER LES NIVEAUX FAVORIS
                liste=[]
                query="SELECT id_niveau from niveaux_favoris WHERE id_utilisateur = '%i'" %(currentID)
                self.cur.execute(query)
                result=self.cur.fetchall()
                for i in result:
                    query="SELECT nomniveau from niveau WHERE id = '%i'" %(i[0])
                    self.cur.execute(query)
                    temp=self.cur.fetchone()
                    liste.append(temp[0])
                dto.setListeNiveauxFavoris(liste)

                #VA CHERCHER LA LISTE DES ARMES
                dictionary={}
                query="SELECT id_arme, nombre from utilisateur_arme WHERE id_utilisateur='%i'" %(currentID)
                self.cur.execute(query)
                result=self.cur.fetchall()
                for i in result:
                    query = "SELECT typearme from arme WHERE id = '%i'" %(i[0])
                    self.cur.execute(query)
                    temp=self.cur.fetchone()
                    dictionary[temp[0]]=i[1]
                dto.setListeArme(dictionary)
                self.fermerBD()
                return dto

    def createUser(self, dto): #Insert un nouvel usager dans la base de donnée (password non crypté pour l'instant, ce sera corriger si le temps le permet)
        if (self.connectionBD()):
            print("Impossible de se connecter à la base de donnée")
            return 0
        else:
            #hashedPassword=bcrypt.hashpw(dto.getPassword(),bcrypt.gensalt())  #UNCOMMENT FOR BCRYPT
            hashedPassword=dto.getPassword() #TO DELETE IF BCRYPT
            query="INSERT INTO utilisateur (nom,prenom,nomJoueur,password,questionSecreteA,reponseQuestionA,questionSecreteB,reponseQuestionB,couleur) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')" %(dto.getNom(),dto.getPrenom(),dto.getNomJoueur(),hashedPassword,dto.getQuestionSecreteA(),dto.getReponseA(),dto.getQuestionSecreteB(),dto.getReponseB(),dto.getColor())
            self.cur.execute(query)
            self.connection.commit()
            self.fermerBD()
            return 1


    def updateUser(self, dto): #Modifie les champ d'un usager dans la base de donnée
        if (self.connectionBD()):
            print("Impossible de se connecter à la base de donnée")
            return 0
        else:
            #UPDATE DES INFORMATIONS DE BASE
            print(dto.getColor())
            query="UPDATE utilisateur SET nom='%s',prenom='%s',password='%s',questionSecreteA='%s',reponseQuestionA='%s',questionSecreteB='%s',reponseQuestionB='%s',couleur='%s' WHERE nomjoueur='%s'" %(dto.getNom(),dto.getPrenom(),dto.getPassword(),dto.getQuestionSecreteA(),dto.getReponseA(),dto.getQuestionSecreteB(),dto.getReponseB(),dto.getColor(),dto.getNomJoueur())
            self.cur.execute(query)

            #Vs chercher le id de l'utilisateur courant
            query="SELECT id from utilisateur WHERE nomJoueur ='%s'" %(dto.getNomJoueur())
            self.cur.execute(query)
            result=self.cur.fetchone()
            currentID=result[0]

            #UPDATE DES NIVEAUX CRÉÉES
            query="DELETE FROM niveaux_crees WHERE id_utilisateur = '%i'" %(currentID)
            self.cur.execute(query)

            listeNiveaux = dto.getListeNiveauxCrees()
            for i in listeNiveaux:
                #Va chercher le id du niveau
                query="SELECT id from niveau WHERE nomniveau = '%s'" %(i)
                self.cur.execute(query)
                result=self.cur.fetchone()

                #On insert dans la table d'association
                query="INSERT INTO niveaux_crees (id_niveau,id_utilisateur) VALUES ('%i','%i')" %(result[0],currentID)
                self.cur.execute(query)

            #UPDATE DES NIVEAUX FAVORIS
            query="DELETE FROM niveaux_favoris WHERE id_utilisateur = '%i'" %(currentID)
            self.cur.execute(query)
            
            listeNiveaux = dto.getListeNiveauxFavoris()
            for i in listeNiveaux:
                #Va chercher le id du niveau
                query="SELECT id from niveau WHERE nomniveau = '%s'" %(i)
                self.cur.execute(query)
                result=self.cur.fetchone()

                #On insert dans la table d'association
                query="INSERT INTO niveaux_favoris (id_niveau,id_utilisateur) VALUES ('%i','%i')" %(result[0],currentID)
                self.cur.execute(query)

            #UPDATE DE LA LISTE DES ARMES
            query="DELETE FROM utilisateur_arme WHERE id_utilisateur = '%i'" %(currentID)
            self.cur.execute(query)
            dictionary=dto.getListeArme()
            for key in dictionary:
                query="SELECT id from arme WHERE typearme='%s'" %(key)
                self.cur.execute(query)
                result=self.cur.fetchone()
                print(currentID,result[0],dictionary[key])
                query="INSERT INTO utilisateur_arme (id_utilisateur,id_arme,nombre) VALUES ('%i','%i','%i')" %(currentID,result[0],dictionary[key])
                self.cur.execute(query)

            self.connection.commit()
            self.fermerBD()
            return 1


    def authentifier(self, username, password): #Verifier si les information match celle de la base de donnée
        valide = False
        if(self.connectionBD()):
            return 3
        try:
            query="SELECT password from utilisateur WHERE nomJoueur='%s'" %(username)
            self.cur.execute(query)
            pw=self.cur.fetchone()
        except:
            print("Nom d'utilisateur ou mot de passe invalide")
            return valide
        #hashedEntry=bcrypt.hashpw(password,pw[0])    #UNCOMMENT FOR BCRYPT
        #valide = hashedEntry == pw[0]    #UNCOMMENT FOR BCRYPT
        valide = password == pw[0]  #TO DELETE IF BCRYPT
        self.fermerBD()
        return valide
