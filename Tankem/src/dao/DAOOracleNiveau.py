#-*- coding: utf-8 -*-

import cx_Oracle
from dto import DTONiveau

class DAOOracleNiveau:

    def connectionBD(self):
        #Cr�ation d'une connection � la BD
        try:
            self.connection = cx_Oracle.connect("e0957119","Baba","10.57.4.60/DECINFO.edu")
        except cx_Oracle.DatabaseError as e:
            error, =e.args
            return 1
            
        #Cr�ation du curseur
        self.cur = self.connection.cursor()

    def fermerBD(self):
        print "Fermeture de la BD"
        self.cur.close()
        self.connection.close()

    def lireListeNomNiveau(self):       
        #Variable lire donn�es de la BD
        if (self.connectionBD()):
            return 1
        else:
            lireDonnees = "SELECT nomNiveau FROM niveau WHERE statutNiveau <> 'Inactif'"
            
            #Ex�cuter une commande du curseur
            self.cur.execute(lireDonnees)
            
            resultat=self.cur.fetchall()

            listeNomsNiveau = []

            for i in resultat:
                listeNomsNiveau.append(i[0])

            dtotemp = DTONiveau.DTONiveau()
            dtotemp.setListeNom(listeNomsNiveau)
            
            #Fermeture du curseur et de la connection
            self.fermerBD()
        
    def lireNiveau(self, nom):
        self.connectionBD()
        print(nom)
        ligneCommande = "SELECT * FROM niveau WHERE nomNiveau = :1"

        try:
            self.cur.execute(ligneCommande,[nom])
        except cx_Oracle.DatabaseError as e:
            error, = e.args
            print(u'Une erreur de commande')
            print(error.code)#pour une recherche google
            print(error.message)# detail
            print(error.context)# quel fonciton a plante

        resultat=self.cur.fetchone()
        dictNiveau={'nomNiveau':resultat[1],'statutNiveau':resultat[3],'delaiApparitionMinimum':resultat[4],'delaiApparitionMaximum':resultat[5],'tank1PosX':resultat[6],'tank1PosY':resultat[7],'tank2PosX':resultat[8],'tank2PosY':resultat[9]}

        dtotemp = DTONiveau.DTONiveau()
        dtotemp.setNiveau(dictNiveau)

        self.fermerBD()

        
    def lireMatrice(self, nom):     
        #Variable lire donn�es de la BD
        self.connectionBD()
        lireDonnees = "SELECT col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12 FROM matrice WHERE id_niveau = (SELECT id FROM niveau WHERE nomNiveau=  :1)"
        
        #Ex�cuter une commande du curseur
        self.cur.execute(lireDonnees,[nom])
        
        resultat=self.cur.fetchall()

        dtotemp = DTONiveau.DTONiveau()
        dtotemp.setMatriceNiveau(resultat)
        
        #Fermeture du curseur et de la connection
        self.fermerBD()
        
        
    def ecrireNiveau(self, dto):
        self.connectionBD()

        dictNiveau=dto.getNiveau()
        statement= "INSERT INTO niveau VALUES (:nomNiveau,SYSDATE,:statutNiveau,:delaiApparitionMinimum,:delaiApparitionMaximum,:tank1PosX,:tank1PosY,:tank2PosX,:tank2PosY, dictNiveau)"

        try:
            self.cur.execute(statement)
        except cx_Oracle.DatabaseError as e:
            error, = e.args
            return 1

        self.connection.commit()
        self.fermerBD()
        
    def ecrireMatrice(self, dto,nomNiv):
        self.connectionBD()
        mat=dto.getMatrice()
        for i in mat:
            dictMat={'i0':i[0],'i1':i[1],'i2':i[2],'i3':i[3],'i4':i[4],'i5':i[5],'i6':i[6],'i7':i[7],'i8':i[8],'i9':i[9],'i10':i[10],'i11':i[11]}
            statement= "INSERT INTO matrice VALUES (SELECT id FROM niveau WHERE nomNiveau = nomNiv,:i0,i[1],i[2],i[3],i[4],i[5],i[6],i[7],i[8],i[9],i[10],i[11], dictMat)"
        try:
            self.cur.execute(statement)
        except cx_Oracle.DatabaseError as e:
            error, = e.args

        self.connection.commit()
        self.fermerBD()