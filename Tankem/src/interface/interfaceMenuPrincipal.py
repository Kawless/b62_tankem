## -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys



from dto import DTONiveau
from dto import dtoUser
from dao import DAOOracleUser


class InterfaceMenuPrincipal(ShowBase):
    def __init__(self):
        self.modeleTank1 = loader.loadModel("../asset/Tank/tank")
        self.modeleTank2 = loader.loadModel("../asset/Tank/tank")
        self.daoUser = DAOOracleUser.DAOOracleUser()
        self.jNom = ["",""]

        self.prep =[False,False]
        #Image d'arrière plan
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menuPrincipal.jpg")

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        #Titre du jeu
        self.textTitre = OnscreenText(text = "Tankem!",
                                      pos = (0,0.6), 
                                      scale = 0.32,
                                      fg=(0.8,0.9,0.7,1),
                                      align=TextNode.ACenter)

        #Boutons
        btnScale = (0.18,0.18)
        text_scale = 0.12
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 0.5
        hauteur = -0.6
        self.b1 = DirectButton(text = ("Jouer", "!", "!", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command=self.selection,
                          pos = (-separation,0,hauteur))


        self.b2 = DirectButton(text = ("Quitter", "Bye!", ":-(", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command = lambda : sys.exit(),
                          pos = (separation,0,hauteur))
        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")



        #-------------------------------MENU SELECTION-----------------------------
        niveau = [] #tableau pour contenir les niveau
        maps = DTONiveau.DTONiveau()

        for i in maps.getListeNom() :
          niveau.append((i))
        
        buttons =[]
        self.var =[0] #variable qui contient le choix de lutilisateur
        
        #creer des radios button pour chaque mappe
        for i in range(len(niveau)):
          buttons.append(DirectRadioButton(text = niveau[i], scale=0.05, pos=(-0.4,0,0),variable=self.var,value=[niveau[i]] ))

        
        buttons.append(DirectRadioButton(text = "Aleatoire",scale=0.05,pos=(-0.4,0,0),variable=self.var,value=["Aleatoire"]))

        for button in buttons:
          button.setOthers(buttons)
     
        self.mapList = DirectScrolledList(
          decButton_pos= (0.35, 0, 0.53),
          decButton_text = "Dec",
          decButton_text_scale = 0.04,
          decButton_borderWidth = (0.005, 0.005),
       
          incButton_pos= (0.35, 0, -0.47),
          incButton_text = "Inc",
          incButton_text_scale = 0.04,
          incButton_borderWidth = (0.005, 0.005),
       
          frameSize = (0.0, 0.7, -0.5, 0.59),
          frameColor = (1,1,1,1),
          pos = (-1.3, 0, 0.1),
          items = buttons,
          numItemsVisible = 8,
          forceHeight = 0.11,
          itemFrame_frameSize = (-0.2, 0.2, -0.82, 0.11),
          itemFrame_pos = (0.35, 0, 0.4),
        )

        self.b3 = DirectButton(text = ("Jouer", "!", "!", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command=self.afficherJoueurSelect,
                          pos = (-separation,0,hauteur))


        self.b4 = DirectButton(text = ("Back", "Menu", ":-(", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command =self.menuPrincipalAffichage,
                          pos = (separation,0,hauteur))

        self.b5 = DirectButton(text = ("Combattre", "!", "!", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command=self.chargeJeu,
                          pos = (-separation,0,hauteur-0.2))


        self.b6 = DirectButton(text = ("Back", "Menu", ":-(", "disabled"),
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command =self.selection,
                          pos = (separation,0,hauteur-0.2))
        #cache le deuxieme menu
        self.b4.hide()
        self.b3.hide()
        self.mapList.hide()
        self.prepConnection()

    def prepConnection(self):
      self.nomJoueur1 = OnscreenText(text = 'Joueur1: ', pos = (-1, 0.8), scale = 0.07,bg =(1,1,1,1),frame=(0,0,0,1))
      self.entryNomJ1 = DirectEntry(text = "",initialText="", numLines = 1,focus=1,
                 pos = (-0.8, 0,0.8), scale = 0.07,command=self.connectionJ1)

      self.nomJoueur2 = OnscreenText(text = 'Joueur2: ', pos = (0.5, 0.8), scale = 0.07,bg =(1,1,1,1),frame=(0,0,0,1))
      self.entryNomJ2 = DirectEntry(text = "",initialText="", numLines = 1,focus=1,
                 pos = (0.7, 0,0.8), scale = 0.07,command=self.connectionJ2)

      self.pwdJoueur1 = OnscreenText(text = 'Password:', pos = (-1,0.6), scale = 0.07,bg =(1,1,1,1),frame=(0,0,0,1))
      self.entryPwdJ1 = DirectEntry(text = "",initialText="", numLines = 1,focus=1,
                 pos = (-0.8, 0,0.6), scale = 0.07,command=self.connectionJ1,obscured=True)
      
      self.pwdJoueur2 = OnscreenText(text = 'Password:', pos = (0.5, 0.6), scale = 0.07,bg =(1,1,1,1),frame=(0,0,0,1))
      self.entryPwdJ2 = DirectEntry(text = "",initialText="", numLines = 1,focus=1,
                 pos = (0.7, 0,0.6), scale = 0.07,command=self.connectionJ2,obscured=True)

      self.message = OnscreenText(text = "Ici cest ou doit se trouver un message derreur", pos = (0,0.4),scale = 0.07,bg=(1,1,1,1),frame=(0,0,0,1))
      self.messCombat = OnscreenText(text = "Joueur 1 VS. Joueur2", pos = (0,-0.25),scale = 0.07,bg=(1,1,1,1),frame=(0,0,0,1))


      self.prim = []
      self.sec = []
      self.buttonPrim1 =[]
      self.buttonSec1 =[]
      self.armeName1 = []
      self.armeQtt1 =[]
      self.buttonPrim2 =[]
      self.buttonSec2 =[]
      self.armeName2 = []
      self.armeQtt2 = []
      self.prim2 = []
      self.sec2 = []
      self.cacherJoueurSelect()

    def connectionJ1(self,ev):
      if self.daoUser.authentifier(self.entryNomJ1.get(),self.entryPwdJ1.get()):
        self.dtoUser1 = self.daoUser.getUser(self.entryNomJ1.get())
        self.prep[0] = True
        self.jNom[0] = self.entryNomJ1.get()
        names =self.dtoUser1.getListeArme()
        names["Canon"] = 999 

        self.prim =["Canon"]
        self.sec =["Canon"]

        self.buttonPrim1 =[]
        self.buttonSec1 =[]
        self.armeQtt1 =[]
        self.armeName1 = []

        j = 0
        for i in names:
          self.buttonPrim1.append(DirectRadioButton(text = "x", scale=0.05, pos=(-0.7,0,0.3-(0.075*j)),variable=self.prim,value=[i]))
          self.buttonSec1.append(DirectRadioButton(text = "x", scale=0.05, pos=(-0.5,0,0.3-(0.075*j)),variable=self.sec,value=[i]))
          self.armeName1.append(DirectLabel(text = i,scale=0.05,pos=(-0.9,0,0.3-(0.075*j))))
          self.armeQtt1.append(DirectLabel(text = str(names[i]), scale=0.05, pos=(-0.3,0,0.3-(0.075*j))))
          j+=1

        for i in self.buttonPrim1:
          i.setOthers(self.buttonPrim1)
        for i in self.buttonSec1:
          i.setOthers(self.buttonSec1)

        for i in self.buttonPrim1:
          i.show()
        for i in self.buttonSec1:
          i.show()
        for i in self.armeName1:
          i.show()
        for i in self.armeQtt1:
          i.show()
        self.message.hide()
        self.verifPrep()
        self.modeleTank1.show()
      else:
        self.messCombat.hide()
        self.modeleTank1.hide()
        self.message.show()
        self.prep[0] = False
        self.b5.hide()
        self.message.setText("Le mot de passe ou le nom de l'utilisateur du joueur1 est incorrecte")

    def verifPrep(self):
      if(self.prep[0] and self.prep[1]):
        self.b5.show()
        self.showTank()
        self.messCombat.setText(self.entryNomJ1.get() +" Vs. " + self.entryNomJ2.get() )
        self.messCombat.show()


    def connectionJ2(self,ev):
      if self.daoUser.authentifier(self.entryNomJ2.get(),self.entryPwdJ2.get()):
          self.dtoUser2 = self.daoUser.getUser(self.entryNomJ2.get())
          self.jNom[1] = self.entryNomJ2.get()
          self.prep[1] = True
          
          names =self.dtoUser2.getListeArme()
          names["Canon"] = 999 

          self.prim2 =["Canon"]
          self.sec2 =["Canon"]

          self.buttonPrim2 =[]
          self.buttonSec2 =[]
          self.armeName2 = []


          j = 0
          for i in names:
            self.buttonPrim2.append(DirectRadioButton(text = "x", scale=0.05, pos=(0.9,0,0.3-(0.075*j)),variable=self.prim2,value=[i]))
            self.buttonSec2.append(DirectRadioButton(text = "x", scale=0.05, pos=(0.7,0,0.3-(0.075*j)),variable=self.sec2,value=[i]))
            self.armeName2.append(DirectLabel(text = i,scale=0.05,pos=(0.5,0,0.3-(0.075*j))))
            self.armeQtt2.append(DirectLabel(text = str(names[i]), scale=0.05, pos=(1.1,0,0.3-(0.075*j))))
            j+=1


          for i in self.buttonPrim2:
            i.setOthers(self.buttonPrim2)
          for i in self.buttonSec2:
            i.setOthers(self.buttonSec2)

          for i in self.buttonPrim2:
            i.show()
          for i in self.buttonSec2:
            i.show()
          for i in self.armeName2:
            i.show()
          for i in self.armeQtt2:
            i.show()
          self.modeleTank2.show()
          self.message.hide()
          self.verifPrep()
      else:
        self.messCombat.hide()
        self.modeleTank2.hide()
        self.message.show()
        self.prep[1] = False
        self.b5.hide()
        self.message.setText("Le mot de passe ou le nom de l'utilisateur du joueur2 est incorrecte")

    def showTank(self):

        self.modeleTank1.setScale(0.75,0.75,0.75)
        color = self.dtoUser1.getColor()[1:]
        r = float(int(color[:2],16))/255
        g = float(int(color[2:4],16))/255
        b = float(int(color[4:],16))/255
        self.modeleTank1.setColorScale(r,g,b,1)
        self.modeleTank1.setPos(-5,30,-3.5)
        self.modeleTank1.setHpr(270,0,0)
        base.cam.attachNewNode(self.modeleTank1.node())
        Interval = self.modeleTank1.hprInterval(2.0, Vec3(-89, 0, 0))
        self.sequence1 = Sequence(Interval)
        self.sequence1.loop()


        self.modeleTank2.setScale(0.75,0.75,0.75)
        color = self.dtoUser2.getColor()[1:]
        r = float(int(color[:2],16))/255
        g = float(int(color[2:4],16))/255
        b = float(int(color[4:],16))/255
        self.modeleTank2.setColorScale(r,g,b,1)
        self.modeleTank2.setPos(5,30,-3.5)
        self.modeleTank2.setHpr(90,0,0)
        base.cam.attachNewNode(self.modeleTank2.node())
        Interval = self.modeleTank2.hprInterval(2.0, Vec3(-270, 0, 0))
        self.sequence2 = Sequence(Interval)
        self.sequence2.loop()

    def cacherJoueurSelect(self):
      self.nomJoueur1.hide()
      self.nomJoueur2.hide()
      self.pwdJoueur1.hide()
      self.pwdJoueur2.hide()
      self.message.hide()
      self.messCombat.hide()
      self.entryNomJ1.hide()
      self.modeleTank1.hide()
      self.modeleTank2.hide()
      self.entryNomJ2.hide()
      self.modeleTank1.hide()
      self.modeleTank2.hide()
      
      self.entryPwdJ1.hide()
      self.entryPwdJ2.hide()
      for i in self.buttonPrim1:
        i.hide()
      for i in self.buttonPrim2:
        i.hide()
      for i in self.buttonSec1:
        i.hide()
      for i in self.buttonSec2:
        i.hide()
      for i in self.armeName1:
        i.hide()
      for i in self.armeQtt1:
        i.hide()
      for i in self.armeQtt2:
        i.hide()
      for i in self.armeName2:
        i.hide()
      self.b5.hide()
      self.b6.hide()

    def afficherJoueurSelect(self):
      self.nomJoueur1.show()
      self.nomJoueur2.show()
      self.pwdJoueur1.show()
      self.pwdJoueur2.show()

      self.entryNomJ1.show()

      self.entryNomJ2.show()
      
      self.entryPwdJ1.show()
      self.entryPwdJ2.show()

      self.b6.show()
      self.cacherMenuPrincipale()
      self.cacherMapSelection()
    def cacher(self):
        #Est esssentiellement un code de "loading"
        self.cacherJoueurSelect()
        #On remet la caméra comme avant
        base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
        #On cache les menus
        self.background.hide()
        self.b1.hide()
        self.b2.hide()
        self.b3.hide()
        self.b4.hide()
        self.mapList.hide()
        self.textTitre.hide()

    def selection(self):
        #cache le menude depart et affiche le menu de selection de niveau
        self.cacherJoueurSelect()
        self.cacherMenuPrincipale()
        self.b4.show()
        self.b3.show()
        self.mapList.show()

    def cacherMenuPrincipale(self):
      self.b1.hide()
      self.b2.hide()

    def cacherMapSelection(self):
      self.b3.hide()
      self.b4.hide()
      self.mapList.hide()

    def menuPrincipalAffichage(self):
        #cache le menu de selection de niveau et affiche le menu de depart
        self.b1.show()
        self.b2.show()

        self.cacherJoueurSelect()
        self.cacherMapSelection()


    def chargeJeu(self):
        #On démarre!
        self.cacherJoueurSelect()
        self.b4.hide()
        self.b3.hide()
        self.mapList.hide()
        info = [self.var,[self.dtoUser1.getColor()[1:],self.dtoUser2.getColor()[1:]],[self.prim,self.prim2],[self.sec,self.sec2],[self.dtoUser1,self.dtoUser2]]
        Sequence(Func(lambda : self.transition.irisOut(0.2)),
                 SoundInterval(self.sound),
                 Func(self.cacher),
                 Func(lambda : messenger.send("DemarrerPartie",info)),
                 Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                 Func(lambda : self.transition.irisIn(0.2))
        ).start()
