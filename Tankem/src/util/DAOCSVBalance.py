# -*- coding: utf-8 -*-
import csv 
from selectionnerFichier import *
import DTOBalance

class DAOCSVBalance():
    def lireCSV(self):
        csv.register_dialect("Pipe",delimiter="|")

        nomFichier = choisirFichierLectureCSV()
        #Ouverture de fichier
        with open(nomFichier,"rb") as file:
            #Utilisation d'un CSV reader
            reader = csv.reader(file,"Pipe")
            next(reader)
            number=1
            liste=[]
            liste.append(1)
            for ligne in reader:
                for cellule in ligne:
                    if number%2 == 1:
                        pass
                    else:
                        liste.append(cellule)
                    number +=1
            print liste
            if len(liste)==26:
                dtotemp=DTOBalance.DTOBalance(liste)
                return dtotemp
            else:
                print "Le fichier csv choisi n'est pas au bon format. Ils est incomplet ou ne contient pas tout le nécessaire"


                    

        #DictReader créé une liste de dictionnaire au lieu d'une liste de liste

    def ecrireCSV(self, dto):
        csv.register_dialect("Pipe",delimiter="|")

        nomFichier = choisirFichierEcritureCSV()
        #Ouverture de fichier
        with open(nomFichier,"wb") as file:
            writer = csv.writer(file,"Pipe")
            writer.writerow(["sep=|"])
            writer.writerow(["vitesseAvancer",dto.vitesseAvancer])
            writer.writerow(["vitesseMaxTourner",dto.vitesseMaxTourner])
            writer.writerow(["pointDeVieMax",dto.pointDeVieMax])
            writer.writerow(["tempsMouvement",dto.tempsMouvement])
            writer.writerow(["canonVitesseBalle",dto.canonVitesseBalle])
            writer.writerow(["canonDelaiArme",dto.canonDelaiArme])
            writer.writerow(["mitrailletteVitesseBalle",dto.mitrailletteVitesseBalle])
            writer.writerow(["mitrailletteDelaiArme",dto.mitrailletteDelaiArme])
            writer.writerow(["grenadeVitesseBalle",dto.grenadeVitesseBalle])
            writer.writerow(["grenadeDelaiArme",dto.grenadeDelaiArme])
            writer.writerow(["shotgunVitesse",dto.shotgunVitesseArme])
            writer.writerow(["shotgunDelaiArme",dto.shotgunDelaiArme])
            writer.writerow(["shotgunOuvertureFusil",dto.shotgunOuvertureFusil])
            writer.writerow(["piegeVitesseBalle",dto.piegeVitesseBalle])
            writer.writerow(["piegeDelaiArme",dto.piegeDelaiArme])
            writer.writerow(["missileGuideVitesseBalle",dto.missileGuideVitesseBalle])
            writer.writerow(["missileGuideDelaiArme",dto.missileGuideDelaiArme])
            writer.writerow(["springVitesseBalle",dto.springVitesseBalle])
            writer.writerow(["springDelaiArme",dto.springDelaiArme])
            writer.writerow(["grosseurExplosion",dto.grosseurExplosion])
            writer.writerow(["messageAccueil",dto.messageAccueuil])
            writer.writerow(["messageAccueilDelai",dto.messageAccueuilDelai])
            writer.writerow(["messageACompteARebourDelai",dto.messageCompteARebourDelai])
            writer.writerow(["messageSignalDebutPartie",dto.messageSignalDebutPartie])
            writer.writerow(["messageSignalFinPartie",dto.messageSignalFinPartie])
        
        return nomFichier