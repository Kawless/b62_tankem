

class DTOBalance:
	def __init__(self,tableauParam):
		self.vitesseAvancer=tableauParam[1];
		self.vitesseMaxTourner=tableauParam[2];
		self.pointDeVieMax=tableauParam[3];
		self.tempsMouvement=tableauParam[4];
		self.canonVitesseBalle=tableauParam[5];
		self.canonDelaiArme=tableauParam[6];
		self.mitrailletteVitesseBalle=tableauParam[7];
		self.mitrailletteDelaiArme=tableauParam[8];
		self.grenadeVitesseBalle=tableauParam[9];
		self.grenadeDelaiArme=tableauParam[10];
		self.shotgunVitesseArme=tableauParam[11];
		self.shotgunDelaiArme=tableauParam[12];
		self.shotgunOuvertureFusil=tableauParam[13];
		self.piegeVitesseBalle=tableauParam[14];
		self.piegeDelaiArme=tableauParam[15];
		self.missileGuideVitesseBalle=tableauParam[16];
		self.missileGuideDelaiArme=tableauParam[17];
		self.springVitesseBalle=tableauParam[18];
		self.springDelaiArme=tableauParam[19];
		self.grosseurExplosion=tableauParam[20];
		self.messageAccueuil=tableauParam[21];
		self.messageAccueuilDelai=tableauParam[22];
		self.messageCompteARebourDelai=tableauParam[23];
		self.messageSignalDebutPartie=tableauParam[24];
		self.messageSignalFinPartie=tableauParam[25];