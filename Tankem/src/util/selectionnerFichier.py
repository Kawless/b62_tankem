from Tkinter import Tk
from tkFileDialog import askopenfilename
from tkFileDialog import asksaveasfilename

def choisirFichierLectureCSV():
    Tk().withdraw()
    nomFicher = askopenfilename(defaultextension = "*.csv", filetypes = [("Ficher CSV",'.csv'),("All files",'*.*')])
    return nomFicher

def choisirFichierEcritureCSV():
    Tk().withdraw()
    nomFicher = asksaveasfilename(defaultextension = "*.csv", filetypes = [("Ficher CSV",'.csv'),("All files",'*.*')], initialdir="Desktop", initialfile="BalanceTankem.csv")
    return nomFicher