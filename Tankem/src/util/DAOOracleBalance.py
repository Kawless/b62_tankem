# -*- coding: utf-8 -*-

import cx_Oracle
from DTOBalance import *

class DAOOracleBalance:
    def lireBalance(self):
        #Création d'une connection à la BD
        try:
            connection = cx_Oracle.connect("e0957119","Baba","10.57.4.60/DECINFO.edu")
        except cx_Oracle.DatabaseError as e:
            error, =e.args
            print u"Erreur de connection à la BD."

        #Création du curseur
        cur = connection.cursor()

        #Variable lire données de la BD
        lireDonnees = "SELECT * from balance"

        #Exécuter une commande du curseur
        cur.execute(lireDonnees)

        dtotemp=cur.fetchone()

        #Fermeture du curseur et de la connection
        cur.close()
        connection.close()

        return DTOBalance(dtotemp)

    def ecrireBalance(self, dto):
        #Connection à la BD
        try:
            connection = cx_Oracle.connect("e0957119","Baba","10.57.4.60/DECINFO.edu")
        except:
            error, =e.args
            print (u"Erreur de connection à la BD.")

        #Création du curseur
        cur = connection.cursor()

        statement= "UPDATE balance SET vitesseAvancer=dto.vitesseAvancer,vitesseMaxTourner=dto.vitesseMaxTourner,pointDeVieMax=dto.pointDeVieMax,tempsMouvement=dto.tempsMouvement,canonVitesseBalle=dto.canonVitesseBalle,canonDelaiArme=dto.canonDelaiArme,mitrailletteVitesseBalle=dto.mitrailletteVitesseBalle,mitrailletteDelaiArme=dto.mitrailletteDelaiArme,grenadeVitesseBalle=dto.grenadeVitesseBalle,grenadeDelaiArme=dto.grenadeDelaiArme,shotgunVitesseBalle=dto.shotgunVitesseBalle,shotgunDelaiArme=dto.shotgunDelaiArme,shotgunOuvertureFusil=dto.shotgunOuvertureFusil,piegeVitesseBalle=dto.piegeVitesseBalle,piegeDelaiArme=dto.piegeDelaiArme,missileGuideVitesseBalle=dto.missileGuideVitesseBalle,missileGuideDelaiArme=dto.missileGuideDelaiArme,springVitesseBalle=dto.springVitesseBalle,springGuideDelaiArme=dto.springGuideDelaiArme,grosseurExplosion=dto.grosseurExplosion,messageAccueil=dto.messageAccueuil,messageAccueilDelai=dto.messageAccueuilDelai,messageACompteARebourDelai=dto.messageACompteARebourDelai,messageSignalDebutPartie=dto.messageSignalDebutPartie,messageSignalFinPartie=dto.messageSignalFinPartie"

        try:
            cur.execute(statement)
        except cx_Oracle.DatabaseError as e:
            error, = e.args
            if error.code == 2290:
                print "Une des valeur ne respecte pas les contraintes de minimum / maximum\nElle s'affichera dans les lignes suivantes du message d'erreur après ck_"
            print(error.code)
            print(error.message)
            print(error.context)

        cur.close()
        connection.close()


