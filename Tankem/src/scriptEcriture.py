# -*- coding: utf-8 -*-

from util import DAOOracleBalance
from util import DAOCSVBalance

daoCSV=DAOCSVBalance.DAOCSVBalance()
daoOracle=DAOOracleBalance.DAOOracleBalance()


try:
	dto = daoCSV.lireCSV()
except:
	print "Le fichier csv choisi n'a pas put être lû."

try:
	daoOracle.ecrireBalance(dto)
	print "Écriture terminée"
except:
	print "Écriture ratée"


