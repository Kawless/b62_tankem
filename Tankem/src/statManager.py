# -*- coding: utf-8 -*-
from util import *

import sys
from direct.showbase.ShowBase import ShowBase
from panda3d.core import *
from direct.interval.IntervalGlobal import *
from dto import DTOStatistique
from dao import DAOOracleStatistique


class StatManager(ShowBase):
    __shared_state = {} #Borg/Singleton design pattern
    def __init__(self):
        self.__dict__ = self.__shared_state #Borg/Singleton design patter
        self.accept("Degat",self.addDamage)
        self.accept("Position",self.addPos)
        self.accept("Tir",self.addTir)
        self.accept("toDto",self.toDto)
        self.cases =[]
        self.pos ={}
        self.arme ={}

    def addPos(self,tank):
        for i in tank:
            self.addingPos(i)

    def addingPos(self,tank):
        id1 = tank.dbID
        pos1 =tank.getPos()
        found = False
        for i in self.cases :
            if(i[0] == pos1[0] and i[1] == pos1[1] and i[2]==id1):
                i[5] += 1
                found = True
        if not found :
            self.cases.append([pos1[0],pos1[1],id1,0,0,1])

    def addTir(self,tank,weapon):
        id1 = tank.nom
        if(weapon != "Canon"):
            if(not (weapon in self.arme ) ):
                self.arme[weapon] = {}
            if(not (id1 in self.arme[weapon] )):
                self.arme[weapon][id1] = [1,0]
            else :
                self.arme[weapon][id1][0]+=1


    def addDamage(self,tank1,tank2,damage,weapon):
        id1 = tank1.dbID
        id2 = tank1.dbID
        pos2 = tank2.getPos()
        pos1 =tank1.getPos()
        found = False
        for i in self.cases :
            if(i[0] == pos1[0] and i[1] == pos1[1] and i[2]==id1):
                i[3] += damage
                found = True
            if(i[0] == pos2[0] and i[1] == pos2[1] and i[2]==id2):
                i[4] += damage
                found = True
        if not found :
            self.cases.append([pos1[0],pos1[1],id1,0,damage,0])
            self.cases.append([pos2[0],pos2[1],id2,0,damage,0])

        if(weapon != "Canon"):
            if(not (weapon in self.arme )):
                self.arme[weapon] = {}
            if( not (id1 in self.arme[weapon]) ):
                self.arme[weapon][id1] = [0,damage]
            else :
                self.arme[weapon][id1][1]+= damage

    def toDto(self,mX,mY,dto):
        gagnant = dto.getGagnant()
        case = []
        for c in self.cases:
            x = c[0]
            y = c[1]
            id = c[2]
            dr = c[3]
            df = c[4]
            temps = c[5]
            case.append([x,y,id,dr,df,temps])

        dto.setStatsCase(case)
        armeStat = {}
        for i in self.arme :
            for nom in self.arme[i]:
                if(nom == gagnant):
                    tir = self.arme[i][nom][0]
                    damage  = self.arme[i][nom][1]
                    armeStat[i] = [tir,damage,0,0]

        for i in self.arme:
            for nom in self.arme[i]:
                if(nom != gagnant):
                    if(not ( i in armeStat )):
                        armeStat[i] = [0,0,0,0]
                    tir = self.arme[i][nom][0]
                    damage  = self.arme[i][nom][1]
                    armeStat[i][2] = tir
                    armeStat[i][3] = damage

        dto.setStatsArmePartie(armeStat)
        dao =DAOOracleStatistique.DAOOracleStatistique()
        dao.setResultatPartie(dto)
