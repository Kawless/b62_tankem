# -*- coding: utf-8 -*-

class DTOUser():

#On utilise pas un borg puisque lorsqu'il y aura deux tanks de "Logged in", on aura besoin de deux dto distinct.
#J'initialise les variables pour être sur que ça ne crash pas si quelqu'un fais un get avant un set.
#Ce sera donc plus facile à utiliser par les autres membres de l'équipe.

    def __init__(self):
        self.nom=""
        self.prenom=""
        self.nomJoueur=""
        self.password=""
        self.questionSecreteA=""
        self.reponseA=""
        self.questionSecreteB=""
        self.reponseB=""
        self.color=""
        self.listeArme=None #Sera un dictionnaire
        self.listeNiveauxCrees=[]
        self.listeNiveauxFavoris=[]

#Nom
    def setNom(self,nom):
        self.nom=nom

    def getNom(self):
        return self.nom

#Prenom
    def setPrenom(self,prenom):
        self.prenom=prenom

    def getPrenom(self):
        return self.prenom

#Nom de joueur
    def setNomJoueur(self, nomJoueur):
        self.nomJoueur=nomJoueur

    def getNomJoueur(self):
        return self.nomJoueur

#Password
    def setPassword(self,password):
        self.password=password

    def getPassword(self):
        return self.password

#Question secrète A
    def setQuestionSecreteA(self,qsa):
        self.questionSecreteA=qsa

    def getQuestionSecreteA(self):
        return self.questionSecreteA

#Reponse A
    def setReponseA(self,rA):
        self.reponseA=rA

    def getReponseA(self):
        return self.reponseA

#Question secrète B
    def setQuestionSecreteB(self,qsb):
        self.questionSecreteB=qsb

    def getQuestionSecreteB(self):
        return self.questionSecreteB

#Reponse B
    def setReponseB(self,rB):
        self.reponseB=rB

    def getReponseB(self):
        return self.reponseB

#Couleur du tank
    def setColor(self, colorCode):
        self.color=colorCode

    def getColor(self):
        return self.color

#Liste des armes disponible
    def setListeArme(self,liste):
        self.listeArme=liste

    def getListeArme(self):
        return self.listeArme

#Liste des niveaux créés
    def setListeNiveauxCrees(self,liste):
        self.listeNiveauxCrees=liste

    def getListeNiveauxCrees(self):
        return self.listeNiveauxCrees

#Liste des niveaux favoris
    def setListeNiveauxFavoris(self,liste):
        self.listeNiveauxFavoris=liste

    def getListeNiveauxFavoris(self):
        return self.listeNiveauxFavoris

#Methode pour tout "setter" en une seule ligne
    def setAll(self,nom,prenom,nomJoueur,password,qsA,rA,qsB,rB,color,listeArme,listeCree,listeFavoris):
        self.setNom(nom)
        self.setPrenom(prenom)
        self.setNomJoueur(nomJoueur)
        self.setPassword(password)
        self.setQuestionSecreteA(qsA)
        self.setReponseA(rA)
        self.setQuestionSecreteB(qsB)
        self.setReponseB(rB)
        self.setColor(color)
        self.setListeArme(listeArme)
        self.setListeNiveauxCrees(listeCree)
        self.setListeNiveauxFavoris(listeFavoris)

'''
#__________TEST DES MÉTHODE, DÉCOMMENTER AU BESOIN________#

dtoTest=DTOUser()

dtoTest.setAll("Cena","John","JOOOOHNN CEEEEENA!","Any hashed password","Les chemises de l'archi du sèche sont-elle sèche ou archi-sèche?","Sèche","What is love?","Baby don't hurt me!","#000000",{'mitraillette':5,'spring':2},["Le Pont", "Le Chateau"],["Les Enfers"])

dtoTest.setNom("Cena")
dtoTest.setPrenom("John")
dtoTest.setNomJoueur("JOOOOHNN CEEEEENA!")
dtoTest.setPassword("Any hashed password")
dtoTest.setQuestionSecreteA("Les chemises de l'archi du sèche sont-elle sèche ou archi-sèche?")
dtoTest.setReponseA("Sèche")
dtoTest.setQuestionSecreteB("What is love?")
dtoTest.setReponseB("Baby don't hurt me!")
dtoTest.setColor("#000000")
dtoTest.setListeArme({'mitraillette':5,'spring':2}) #Normalement, cela contiendrait toutes les armes...
dtoTest.setListeNiveauxCrees(["Le Pont", "Le Chateau"])
dtoTest.setListeNiveauxFavoris(["Les Enfers"])

print("Résultat du test : ")
print(dtoTest.getPrenom())
print(dtoTest.getNom())
print(dtoTest.getNomJoueur())
print(dtoTest.getPassword())
print(dtoTest.getQuestionSecreteA())
print(dtoTest.getReponseA())
print(dtoTest.getQuestionSecreteB())
print(dtoTest.getReponseB())
print(dtoTest.getColor())
print(dtoTest.getListeArme()['spring'])
print(dtoTest.getListeNiveauxCrees())
print(dtoTest.getListeNiveauxFavoris())
'''