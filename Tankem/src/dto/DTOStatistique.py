# -*- coding: utf-8 -*-


class DTOStatistique():

    def __init__(self):
        #Valeurs necessaire pour chaque partie
        self.abandon1=0
        self.abandon2=0
        self.gagnant=0 #id du gagnant
        self.perdant=0 #id du perdant
        self.statsCase=[] #liste de liste contenant les statistique pour chacune des cases sous le format [[x,y,id_joueur,dommage_recu,dommage_infliger,temps]]
        self.statsArmePartie={} #Dictionnaire contenant les stats des armes pour les deux joueur sous le format {nomArme : [nbTirGagnant,qteDommageGagnant,nbTirPerdant,qteDommagePerdant],}
        self.nomNiveau=""
        self.datePartie=""

        #Valeurs necessaire au statistiques glabale des joueurs
        self.nbrVictoire=0
        self.nbrDefaite=0
        self.nbrAbandon=0
        self.nbrNiveauxCree=0
        self.statsArmeJoueur={} #dictionnaire sous le format {nomArme :[tirs,degat]}

    #SETTERS AND GETTERS

    def getAbandon1(self):
        return self.abandon1

    def setAbandon1(self,value):
        self.abandon1=value

    def getAbandon2(self):
        return self.abandon2

    def setAbandon2(self,value):
        self.abandon2=value

    def getGagnant(self):
        return self.gagnant

    def setGagnant(self, value):
        self.gagnant=value

    def getPerdant(self):
        return self.perdant

    def setPerdant(self, value):
        self.perdant=value

    def getStatsCase(self):
        return self.statsCase

    def setStatsCase(self,value):
        self.statsCase=value

    def getStatsArmePartie(self):
        return self.statsArmePartie

    def setStatsArmePartie(self,value):
        self.statArmePartie=value

    def getNomNiveau(self):
        return self.nomNiveau

    def setNomNiveau(self,value):
        self.nomNiveau=value

    def getDatePartie(self):
        return self.datePartie

    def setDatePartie(self,value):
        self.datePartie=value

    def getNbrVictoire(self):
        return self.nbrVictoire

    def setNbrVictoire(self,value):
        self.nbrVictoire=value

    def getNbrDefaite(self):
        return self.nbrDefaite

    def setNbrDefaite(self,value):
        self.nbrDefaite=value

    def getNbrAbandon(self):
        return self.nbrAbandon

    def setNbrAbandon(self,value):
        self.nbrAbandon=value

    def getNbrNiveauxCree(self):
        return self.nbrNiveauxCree

    def setNbrNiveauxCree(self,value):
        self.nbrNiveauxCree=value

    def getStatsArmeJoueur(self):
        return self.statsArmeJoueur

    def setStatsArmeJoueur(self,value):
        self.statsArmeJoueur=value