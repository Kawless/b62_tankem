## -*- coding: utf-8 -*-

class DTONiveau:
	__shared_state = {} #Borg/Singleton design pattern
	def __init__(self):
		self.__dict__ = self.__shared_state #Borg/Singleton design pattern

	def setListeNom(self, listeNom): 
		self.ListeNom=listeNom

	def getListeNom(self):
		return(self.ListeNom)

	def setNiveau(self, dictNiveau):
		self.dictNiveau=dictNiveau

	def getNiveau(self):
		return(self.dictNiveau)

	def setMatriceNiveau(self, mat):
		self.matriceNiveau=mat

	def getMatriceNiveau(self):
		return(self.matriceNiveau)