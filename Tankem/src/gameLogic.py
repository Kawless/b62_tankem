# -*- coding: utf-8 -*-
from util import *

from direct.showbase.ShowBase import ShowBase
from panda3d.core import *
from panda3d.bullet import BulletWorld
from panda3d.bullet import BulletPlaneShape
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletDebugNode
import random

#Modules de notre jeu
from map import Map
from inputManager import InputManager
from interface import *

from dto import DTONiveau
from dao import DAOOracleNiveau
from dao import DAOOracleUser
from dto import DTOStatistique
from dao import DAOOracleStatistique

import statManager

from direct.task import Task

#Classe qui gère les phases du jeu (Menu, début, pause, fin de partie)
class GameLogic(ShowBase):
    def __init__(self,pandaBase):
        stat = statManager.StatManager()
        self.pandaBase = pandaBase
        self.dto=DTONiveau.DTONiveau()
        self.dao=DAOOracleNiveau.DAOOracleNiveau()
        self.connectionReussi = 1
        if (self.dao.lireListeNomNiveau()==None):
            self.connectionReussi = 0 
        self.accept("DemarrerPartie",self.startGame)
        self.accept("tankPerdant", self.victoire)
    
    def gatherInfo(self,task):
        tank = self.map.getTank()
        messenger.send("Position",[tank] )
        return task.again



    def victoire(self,identifiant1):
        identifiant = identifiant1
        for i in self.identifiant:
            if(i != identifiant1):
                identifiant = i
                break

        armes = ["Grenade","Mitraillette","Piege","Shotgun","Guide","Spring"]
        nom = armes[random.randint(0,len(armes)-1)]

        listArme =identifiant.getListeArme()
        found = False
        for i in listArme:
            if i == nom:
                listArme[i] = listArme[i]+1
                found = True
                break
        if(not found):
            listArme[nom] = 1

        winner = self.daoStat.getIdJoueur(identifiant1.getNomJoueur())
        loser = self.daoStat.getIdJoueur(identifiant.getNomJoueur())

        
        print("winner: ", winner)
        print("loser: ", loser)
        self.dtoStat.setGagnant(winner)
        self.dtoStat.setPerdant(loser)
        daoUser = DAOOracleUser.DAOOracleUser()

        messenger.send("toDto", [self.map.map_nb_tuile_x,self.map.map_nb_tuile_y,self.dtoStat])

        #daoUser.updateUser(identifiant)


    def setup(self,map_id,color,armeP,armeS,noms):
        self.identifiant = noms
        self.setupBulletPhysics()

        self.setupCamera()
        self.setupMap(map_id)
        self.setupLightAndShadow()

        self.daoStat = DAOOracleStatistique.DAOOracleStatistique()
        ids = []
        ids.append(self.daoStat.getIdJoueur(noms[0].getNomJoueur()))
        ids.append(self.daoStat.getIdJoueur(noms[1].getNomJoueur()))
        #Création d'une carte de base
        #
        if(map_id == "Aléatoire"):
            self.map.construireMapHasard()
        else:
            self.dtoStat = DTOStatistique.DTOStatistique()
            self.dtoStat.setAbandon1(ids[0])
            self.dtoStat.setAbandon2(ids[1])
            self.dtoStat.setNomNiveau(map_id)
            self.daoStat.creerPartie(self.dtoStat)
            self.map.construireMap(color,armeP,armeS,noms,ids)
        

        #A besoin des éléments de la map
        self.setupControle()
        self.setupInterface()

        #Fonction d'optimisation
        #DOIVENT ÊTRE APPELÉE APRÈS LA CRÉATION DE LA CARTE
        #Ça va prendre les modèles qui ne bougent pas et en faire un seul gros
        #en faisant un seul gros noeud avec
        self.map.figeObjetImmobile()

        #DEBUG: Décommenter pour affiche la hiérarchie
        #self.pandaBase.startDirect()

        messenger.send("ChargementTermine")

    def startGame(self,Map_ID,color,armePrim,armeSec,noms):


        self.setup(Map_ID[0],color,armePrim,armeSec,noms)

        #On démarrer l'effet du compte à rebour.
        #La fonction callBackDebutPartie sera appelée à la fin

        longueurCompte=3 if balance.dto == 0 else balance.dto.messageCompteARebourDelai #ADD:  Garde la valeur par default si la lecture de bd a raté, sinon, prend la valuer de la bd 
        longueurMessage=3 if balance.dto == 0 else balance.dto.messageAccueuilDelai #ADD:  Garde la valeur par default si la lecture de bd a raté, sinon, prend la valuer de la bd 
        message="Problème de connection. Configuration par défaut utilisée." if balance.dto == 0 else balance.dto.messageAccueuil #ADD:  Garde la valeur par default si la lecture de bd a raté, sinon, prend la valuer de la bd 

        self.interfaceMessage.effectCountDownStart(longueurCompte,self.callBackDebutPartie) #Change: longueuer du compte dans une variable
        self.interfaceMessage.effectMessageGeneral(message,longueurMessage)
        taskMgr.doMethodLater(1, self.gatherInfo, 'gatherInformation')

    def setupBulletPhysics(self):
        debugNode = BulletDebugNode('Debug')
        debugNode.showWireframe(True)
        debugNode.showConstraints(True)
        debugNode.showBoundingBoxes(False)
        debugNode.showNormals(False)
        self.debugNP = render.attachNewNode(debugNode)

        self.mondePhysique = BulletWorld()
        self.mondePhysique.setGravity(Vec3(0, 0, -9.81))
        self.mondePhysique.setDebugNode(self.debugNP.node())
        taskMgr.add(self.updatePhysics, "updatePhysics")

        taskMgr.add(self.updateCarte, "updateCarte")

    def setupCamera(self):
        #On doit désactiver le contrôle par défaut de la caméra autrement on ne peut pas la positionner et l'orienter
        self.pandaBase.disableMouse()

        #Le flag pour savoir si la souris est activée ou non n'est pas accessible
        #Petit fail de Panda3D
        taskMgr.add(self.updateCamera, "updateCamera")
        self.setupTransformCamera()


    def setupTransformCamera(self):
        #Défini la position et l'orientation de la caméra
        self.positionBaseCamera = Vec3(0,-18,32)
        camera.setPos(self.positionBaseCamera)
        #On dit à la caméra de regarder l'origine (point 0,0,0)
        camera.lookAt(render)

    def setupMap(self,map_ID):
        self.map = Map(self.mondePhysique,map_ID)
        #On construire la carte comme une coquille, de l'extérieur à l'intérieur
        #Décor et ciel
        self.map.construireDecor(camera)
        #Plancher de la carte
        self.map.construirePlancher()
        #Murs et éléments de la map

    def setupLightAndShadow(self):
        #Lumière du skybox
        plight = PointLight('Lumiere ponctuelle')
        plight.setColor(VBase4(1,1,1,1))
        plnp = render.attachNewNode(plight)
        plnp.setPos(0,0,0)
        camera.setLight(plnp)

        #Simule le soleil avec un angle
        dlight = DirectionalLight('Lumiere Directionnelle')
        dlight.setColor(VBase4(0.8, 0.8, 0.6, 1))
        dlight.get_lens().set_fov(75);
        dlight.get_lens().set_near_far(0.1, 60);
        dlight.get_lens().set_film_size(30,30);
        dlnp = render.attachNewNode(dlight)
        dlnp.setPos(Vec3(-2,-2,7))
        dlnp.lookAt(render)
        render.setLight(dlnp)

        #Lumière ambiante
        alight = AmbientLight('Lumiere ambiante')
        alight.setColor(VBase4(0.25, 0.25, 0.25, 1))
        alnp  = render.attachNewNode(alight)
        render.setLight(alnp)

        #Ne pas modifier la valeur 1024 sous peine d'avoir un jeu laid ou qui lag
        dlight.setShadowCaster(True, 1024,1024)
        #On doit activer l'ombre sur les modèles
        render.setShaderAuto()

    def setupControle(self,):
        #Créer le contrôle
        #A besoin de la liste de tank pour relayer correctement le contrôle
        self.inputManager = InputManager(self.map.listTank,self.debugNP,self.pandaBase)
        self.accept("initCam",self.setupTransformCamera)

    def setupInterface(self):
        self.interfaceTank = []
        self.interfaceTank.append(InterfaceTank(0,self.map.listTank[0].couleur))
        self.interfaceTank.append(InterfaceTank(1,self.map.listTank[1].couleur))

        self.interfaceMessage = InterfaceMessage()

    def callBackDebutPartie(self):
        #Quand le message d'introduction est terminé, on permet aux tanks de bouger
        self.inputManager.debuterControle()

    #Mise à jour du moteur de physique
    def updateCamera(self,task):
        #On ne touche pas à la caméra si on est en mode debug
        if(self.inputManager.mouseEnabled):
            return task.cont

        vecTotal = Vec3(0,0,0)
        distanceRatio = 1.0
        if (len(self.map.listTank) != 0):
            for tank in self.map.listTank:
                vecTotal += tank.noeudPhysique.getPos()
            vecTotal = vecTotal/len(self.map.listTank)

        vecTotal.setZ(0)
        camera.setPos(vecTotal + self.positionBaseCamera)
        return task.cont

    #Mise à jour du moteur de physique
    def updatePhysics(self,task):
        dt = globalClock.getDt()
        messenger.send("appliquerForce")
        self.mondePhysique.doPhysics(dt)
        #print(len(self.mondePhysique.getManifolds()))

        #Analyse de toutes les collisions
        for entrelacement in self.mondePhysique.getManifolds():
            node0 = entrelacement.getNode0()
            node1 = entrelacement.getNode1()
            self.map.traiterCollision(node0, node1)
        return task.cont

    def updateCarte(self,task):
        self.map.update()
        return task.cont
