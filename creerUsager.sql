DECLARE
	compteur INTEGER;
	type nomsArmes IS VARRAY(10) OF VARCHAR(15);
	listeArmes nomsArmes;
	type id_utilisateur IS NUMBER;
	idUtilisateur id_utilisateur;
	type id_arme IS NUMBER;
	idArme id_arme;
	type nomUtilisateur IS VARRAY(10) OF VARCHAR(40);
	listeNomUtilisateur nomUtilisateur;
	type prenomUtilisateur IS VARRAY(10) OF VARCHAR(40);
	listePrenomUtilisateur prenomUtilisateur;
	type passwordUtilisateur IS VARCHAR(40);
	type questionSecreteAUtilisateur IS VARRAY(10) OF VARCHAR(100);
	listeQuestionSecreteA questionSecreteAUtilisateur;
	type reponseQuestionAUtilisateur IS VARRAY(10) OF VARCHAR(100);
	listeReponseQuestionA reponseQuestionAUtilisateur;
	type questionSecreteBUtilisateur IS VARRAY(10) OF VARCHAR(100);
	listeQuestionSecreteB questionSecreteBUtilisateur;
	type reponseQuestionBUtilisateur IS VARRAY(10) OF VARCHAR(100);
	listeReponseQuestionB reponseQuestionBUtilisateur;
	type couleurUtilisateur IS VARRAY(10) OF VARCHAR(20);
	listePasswordUtilisateur passwordUtilisateur;
CREATE OR REPLACE PROCEDURE creerUsager(nomJoueur IN VARCHAR2)
AS
BEGIN
	compteur := 0;
	listeArmes := nomsArmes('Grenade','Mitraillette','Piege','Shotgun','Guide','Spring','Grenade','Mitraillette','Piege','Shotgun');
	listeNomUtilisateur := nomUtilisateur('Courcelles','Cardin','Laplante-Turpin','Zhang','Michaud','Tremblay','Bisaillon','Lauzon','Laporte','Chalifour');
	listePrenomUtilisateur := prenomUtilisateur('Nicolas','Richard','Alexandre','Éliza','Anne','Thomas','Lyne','Eric','Simon','Samuel');
	passwordUtilisateur := dbcrypt.setkey('AAAaaa111'); /*https://dbaportal.eu/2010/08/17/dbms_crypto-example/*/
	listeQuestionSecreteA := questionSecreteAUtilisateur('Quel est le nom de votre chat?',
														 'Quel est le nom de votre chien?',
														 'Quelle est votre voiture prefere?',
														 'Quelle est votre equipe de hockey prefere?',
														 'Quelle est votre equipe de football prefere?',
														 'Quelle est la date de naissance de votre mere?',
														 'Quelle est la date de naissance de votre pere?',
														 'Quelle est le prenom de votre mere?',
														 'Quelle est le prenom de votre mere?',
														 'Quelle est votre sorte de chips prefere?');
														 
	listeReponseSecreteA := reponseSecreteAUtilisateur('Pendore',
													   'Maya',
													   'Subaru Impreza',
													   'Canadiens de Montreal',
													   'Denver Broncos',
													   '1963',
													   '1963',
													   'Lyne',
													   'Christian',
													   'Doritos');
													   
   listeQuestionSecreteB := questionSecreteBUtilisateur('Quel est le nom de votre hamster?',
														'Quel est votre liqeure prefere?',
														'Quelle est votre avion prefere?',
														'Quelle est votre joueur de tennis prefere',
														'Quelle est votre equipe de baseball prefere?',
														'Quelle est le prenom de votre grand-mere paternelle?',
														'Quelle est le prenom de votre grand-pere paternelle?',
														'Quelle est le prenom de votre grand-mere maternelle?',
														'Quelle est le prenom de votre grand-mere maternelle?',
														'Quelle est votre sorte de pizza prefere');
													   
   listeReponseSecreteB := reponseSecreteBUtilisateur('Rex',
													  'Pepsi',
													  'boeing 777',
													  'Roger Federer',
													  'Blue jays',
													  'Jeanette',
													  'Joseph',
													  'Yvette',
													  'Rene',
													  'Deluxe');
	FOR elem IN mod(abs(dbms_random.random),10)
	LOOP
	compteur := compteur + 1;
	idUtilisateur := elem;
	idArme := elem;
	INSERT INTO utilisateur VALUES (listeNomUtilisateur(elem),
									listePrenomUtilisateur(elem),
									nomJoueur,
									passwordUtilisateur,
									listeQuestionSecreteA(elem),
									listeReponseSecreteA(elem),
									listeQuestionSecreteB(elem),
									listeReponseSecreteB(elem));
	INSERT INTO arme VALUES (listeArmes(elem));	
	INSERT INTO utilisateur_arme VALUES (idUtilisateur,idArme,compteur);
	END LOOP;
	
END;