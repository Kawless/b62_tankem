DROP TABLE joueur_arme_partie;
DROP TABLE partie_case;
DROP TABLE partie;

CREATE TABLE partie(
	id NUMBER GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) PRIMARY KEY,
	id_niveau NUMBER REFERENCES niveau(id),
	date_partie DATE NOT NULL,
	id_gagnant NUMBER REFERENCES utilisateur(id),
	id_perdant NUMBER REFERENCES utilisateur(id),
	abandon1 NUMBER REFERENCES utilisateur(id),
	abandon2 NUMBER REFERENCES utilisateur(id)
);

CREATE TABLE partie_case(
	id_partie NUMBER REFERENCES partie(id),
	x NUMBER NOT NULL,
	y NUMBER NOT NULL,
	id_joueur NUMBER REFERENCES utilisateur(id),
	dommage_recu NUMBER DEFAULT 0, 
	dommage_infliger NUMBER DEFAULT 0,
	temps FLOAT,
	CONSTRAINT pk_partie_case PRIMARY KEY (id_partie,x,y,id_joueur)
);

CREATE TABLE joueur_arme_partie(
	id_joueur NUMBER REFERENCES utilisateur(id),
	id_arme NUMBER REFERENCES arme(id),
	id_partie NUMBER REFERENCES partie(id),
	tir NUMBER,
	dommage FLOAT,
	CONSTRAINT pk_joueur_arme_partie PRIMARY KEY (id_joueur, id_arme, id_partie)
);