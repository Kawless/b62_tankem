set serveroutput on
DECLARE
  TYPE simplearray IS VARRAY(10) of VARCHAR2(25);
  arrayPrenom simplearray;
  arrayNom simplearray;
  username VARCHAR2(10);
  pass VARCHAR2(10);
  arrayQ simplearray;
  arrayRep simplearray;
  arrayCouleur simplearray;
  idjoueur NUMBER;
BEGIN
arrayPrenom := simplearray('Bob', 'Roger' , 'Robert' , 'Jay' , 'Simon' , 'Nicolas' , 'Richard' , 'Alexandre' , 'Marc' , 'Pedro');
arrayNom := simplearray('Smith', 'Jones' , 'Brosseau' , 'Dutrizac' , 'MaoChe' , 'Wong' , 'Lafontaine' , 'Tremblay' , 'Laplante' , 'Turpin');
arrayQ := simplearray('Nom du chien?', 'Nom du chat?' , 'Voiture prefere?' , 'Sport favori?' , 'Nom de jeune fille de votre mere?' , 'Chips favorite?' , 'Premier professeur?' , 'Equipe favorite' , 'Date de naissance de votre mere?' , 'Nom de votre oiseau?');
arrayRep := simplearray('John Cena', 'Maximilien' , '11 septembre 2001' , 'Lays au ketchup' , 'Hockey' , 'Canadien' , 'MLG' , 'Patate pille' , 'Shia leboeuf' , 'Johnnhy');
arrayCouleur := simplearray('#123456', '#bf34ac' , '#dddddd' , '#6565ff' , '#000000' , '#aaaaaa' , '#ac5342' , '#55562a' , '#abba25' , '#ffb4ac');
username := 'aladin';
pass := 'AAAaaa111';

INSERT INTO utilisateur VALUES(arrayNom[mod(abs(dbms_random.random,10))],arrayPrenom[mod(abs(dbms_random.random,10))],username,pass,arrayQ[mod(abs(dbms_random.random,10))],arrayRep[mod(abs(dbms_random.random,10))],arrayQ[mod(abs(dbms_random.random,10))],arrayRep[mod(abs(dbms_random.random,10))],arrayCouleur[mod(abs(dbms_random.random,10))]);
COMMIT;

SELECT id INTO idjoueur FROM utilisateur [WHERE nomJoueur=username];
INSERT INTO utilisateur_arme VALUES (idjoueur,1,mod(abs(dbms_random.random,10)));
INSERT INTO utilisateur_arme VALUES (idjoueur,2,mod(abs(dbms_random.random,10)));
INSERT INTO utilisateur_arme VALUES (idjoueur,3,mod(abs(dbms_random.random,10)));
INSERT INTO utilisateur_arme VALUES (idjoueur,4,mod(abs(dbms_random.random,10)));
INSERT INTO utilisateur_arme VALUES (idjoueur,5,mod(abs(dbms_random.random,10)));
INSERT INTO utilisateur_arme VALUES (idjoueur,6,mod(abs(dbms_random.random,10)));
COMMIT;
END;


