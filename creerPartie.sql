set serveroutput on
DECLARE
/*INFO BD utilisateur et niveau*/
nomJoueur1BD IS VARCHAR2(40);
nomJoueur2BD IS VARCHAR2(40);
nomNiveauBD IS VARCHAR2(40);

/*INFO BD partie Joueur1*/
idNiveauJ1BD IS NUMBER; /*fk*/
datePartieJ1BD IS DATE;
idGagnantJ1BD IS NUMBER;/*fk*/
idPerdantJ1BD IS NUMBER;/*fk*/
abandonJ1BD IS NUMBER; /*fk*/

/*INFO BD partie_case Joueur1*/
xJ1BD IS NUMBER;
yJ1BD IS NUMBER;
id_joueurPCJ1BD NUMBER; /*fk*/
dommage_recuJ1BD IS NUMBER;
dommage_infligerJ1BD IS NUMBER; /*Default 0*/
tempsJ1BD IS FLOAT;

/*INFO BD partie Joueur2*/
idNiveauJ2BD IS NUMBER; /*fk*/
datePartieJ2BD IS DATE;
idGagnantJ2BD IS NUMBER;/*fk*/
idPerdantJ2BD IS NUMBER;/*fk*/
abandonJ2BD IS NUMBER; /*fk*/

/*INFO BD partie_case Joueur2*/
xJ2BD IS NUMBER;
yJ2BD IS NUMBER;
id_joueurPCJ2BD NUMBER; /*fk*/
dommage_recuJ2BD IS NUMBER;
dommage_infligerJ2BD IS NUMBER; /*Default 0*/
tempsJ2BD IS FLOAT;

/*INFO BD joueur_arme_partie Joueur1*/
id_joueurJ1BD IS NUMBER; /*fk*/
id_armeJ1BD IS NUMBER; /*fk*/
id_partieJ1BD IS NUMBER; /*fk*/
tirJ1BD IS NUMBER;
dommageJ1BD IS FLOAT;
/*INFO BD joueur_arme_partie Joueur2*/

/*ROW COUNT pour compteur les colonnes de niveau*/
rowCountNiveau IS NUMBER;

/*ROW COUNT pour compteur la quantite d'armes*/
totalArmes IS NUMBER;

/*REFERENCE id niveau pour partie*/
referenceNiveau IS NUMBER;

CREATE OR REPLACE PROCEDURE creerPartie(nomJoueur1 IN VARCHAR2, nomJoueur2 IN VARCHAR2, nomNiveau1 IN VARCHAR2)
AS
BEGIN
/*ROW COUNT pour compteur les colonnes niveau*/
rowCountNiveau := SELECT COUNT(SUBSTR(col)) FROM niveau;
 
/*ROW COUNT pour compteur la quantite d'armes*/
totalArmes := SELECT nombre FROM utilisateur_arme;

/*REFERENCE id niveau pour partie*/
referenceNiveau := SELECT id FROM niveau [WHERE nomNiveau=nomNiveau1];
 
/*INFO BD utilisateur et niveau*/
nomJoueur1BD := SELECT nomJoueur FROM utilisateur [WHERE nomJoueur=nomJoueur1];
nomJoueur2BD := SELECT nomJoueur FROM utilisateur [WHERE nomJoueur=nomJoueur2];
nomNiveauBD := SELECT nomNiveau FROM niveau [WHERE id=nomNiveau1];

/*INFO BD partie Joueur1*/
idNiveauJ1BD := SELECT id FROM niveau [WHERE nomNiveau=nomNiveau1];
datePartieJ1BD := SYSDATE;
idGagnantJ1BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur1];
idPerdantJ1BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur1];
abandonJ1BD := 1;

/*INFO BD partie Joueur2*/
idNiveauJ2BD := SELECT id FROM niveau [WHERE nomNiveau=nomNiveau1];
datePartieJ2BD := SYSDATE;
idGagnantJ2BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur2];
idPerdantJ2BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur2];
abandonJ2BD := 0;

/*INFO BD partie_case Joueur1*/
xJ1BD := (abs(dbms_random.random),rowCount);
yJ1BD := (abs(dbms_random.random),rowCount);
id_joueurPCJ1BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur1];
dommage_recuJ1BD := (abs(dbms_random.random),1000);
dommage_infligerJ1BD := (abs(dbms_random.random),1000);
tempsJ1BD := 60;

/*INFO BD partie_case Joueur2*/
xJ2BD := (abs(dbms_random.random),rowCount);
yJ2BD := (abs(dbms_random.random),rowCount);
id_joueurPCJ2BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur1];
dommage_recuJ2BD := (abs(dbms_random.random),1000);
dommage_infligerJ2BD := (abs(dbms_random.random),1000);
tempsJ2BD := 60;

/*INFO BD joueur_arme_partie Joueur1*/
id_joueurJ1BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur1];
id_armeJ1BD := SELECT id FROM arme [WHERE id=(abs(dbms_random.random),totalArmes];
id_partieJ1BD := SELECT id FROM partie [WHERE id=referenceNiveau];
tirJ1BD := (abs(dbms_random.random),1000);
dommageJ1BD := (abs(dbms_random.random),1000);

/*INFO BD joueur_arme_partie Joueur2*/
id_joueurJ2BD := SELECT id FROM utilisateur [WHERE nomJoueur=nomJoueur2];
id_armeJ2BD := SELECT id FROM arme [WHERE id=(abs(dbms_random.random),totalArmes];
id_partieJ2BD := SELECT id FROM partie [WHERE id=referenceNiveau];
tirJ2BD := (abs(dbms_random.random),1000);
dommageJ2BD := (abs(dbms_random.random),1000);


	IF nomJoueur1BD AND nomJoueur2BD AND choixNiveauBD != null THEN
	{
		INSERT INTO partie VALUES (idNiveauJ1BD,
								   datePartieJ1BD,
								   idGagnantJ1BD,
								   idPerdantJ1BD,
								   abandonJ1BD);
								   
		INSERT INTO partie VALUES (idNiveauJ2BD,
								   datePartieJ2BD,
								   idGagnantJ2BD,
								   idPerdantJ2BD,
								   abandonJ2BD);
								   
		INSERT INTO partie_case VALUES (xJ1BD,
										yJ1BD,
										id_joueurPCJ1BD,
										dommage_recuJ1BD,
										dommage_infligerJ1BD,
										tempsJ1BD);
										
		INSERT INTO partie_case VALUES (xJ2BD,
										yJ2BD,
										id_joueurPCJ2BD,
										dommage_recuJ2BD,
										dommage_infligerJ2BD,
										tempsJ2BD);
										
		INSERT INTO joueur_arme_partie VALUES (id_joueurJ1BD,
											   id_armeJ1BD,
											   id_partieJ1BD,
											   tirJ1BD,
											   dommageJ1BD);
											   
		INSERT INTO joueur_arme_partie VALUES (id_joueurJ2BD,
											   id_armeJ2BD,
											   id_partieJ2BD,
											   tirJ2BD,
											   dommageJ2BD);
											   
		dbms_output.put_line(CONCAT('Voici la partie creer par le joueur1 : ',
									nomJoueur1, 'et le joueur2 : ', nomJoueur2,
									'dans le niveau');
	}
	ELSE
	{
		dbms_output.put_line('ERREUR avec votre query : Soit que votre nom de joueur1
							 n"'"existe pas, ou que votre nom de joueur2 n"'"existe pas,
							 ou que votre nom de niveau est invalide.');
	}
	END IF;
END;