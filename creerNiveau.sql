set serveroutput on
DECLARE
choixNiveauBD IS VARCHAR2(40);
nomJoueurBD IS VARCHAR2(40);

FOR i in 1 .. 5 LOOP
/*INFO BD niveau1*/
CONCAT(nomNiveauBDn,i) IS VARCHAR2(20);
CONCAT(dateCreationBDn,i) IS DATE;
CONCAT(statutNiveauBDn,i) IS VARCHAR2(20);
CONCAT(delaiApparitionMinimumBDn,i) IS NUMBER;
CONCAT(delaiApparitionMaximumBDn,i) IS NUMBER;
CONCAT(tank1PosXBDn,i) IS NUMBER;
CONCAT(tank1PosYBDn,i) IS NUMBER;
CONCAT(tank2PosXBDn,i) IS NUMBER;
CONCAT(tank2PosYBDn,i) IS NUMBER;

/*INFO BD matrice1 de niveau1*/
CONCAT(id_niveauBDn,i) IS NUMBER; /*fk*/
CONCAT(col1BDn,i) IS NUMBER;
CONCAT(col2BDn,i) IS NUMBER;
CONCAT(col3BDn,i) IS NUMBER;
CONCAT(col4BDn,i) IS NUMBER;
CONCAT(col5BDn,i) IS NUMBER;
CONCAT(col6BDn,i) IS NUMBER;
CONCAT(col7BDn,i) IS NUMBER;
CONCAT(col8BDn,i) IS NUMBER;
CONCAT(col9BDn,i) IS NUMBER;
CONCAT(col10BDn,i) IS NUMBER;
CONCAT(col11BDn,i) IS NUMBER;
CONCAT(col12BDn,i) IS NUMBER;
END LOOP;

/*
/*INFO BD niveau2
nomNiveauBDn2 IS VARCHAR2(20);
dateCreationBDn2 IS DATE;
statutNiveauBDn2 IS VARCHAR2(20);
delaiApparitionMinimumBDn2 IS NUMBER;
delaiApparitionMaximumBDn2 IS NUMBER;
tank1PosXBDn2 IS NUMBER;
tank1PosYBDn2 IS NUMBER;
tank2PosXBDn2 IS NUMBER;
tank2PosYBDn2 IS NUMBER;

/*INFO BD matrice1 de niveau2
id_niveauBDn2 IS NUMBER; /*fk
col1BDn2 IS NUMBER;
col2BDn2 IS NUMBER;
col3BDn2 IS NUMBER;
col4BDn2 IS NUMBER;
col5BDn2 IS NUMBER;
col6BDn2 IS NUMBER;
col7BDn2 IS NUMBER;
col8BDn2 IS NUMBER;
col9BDn2 IS NUMBER;
col10BDn2 IS NUMBER;
col11BDn2 IS NUMBER;
col12BDn2 IS NUMBER;

/*INFO BD niveau3
nomNiveauBDn3 IS VARCHAR2(20);
dateCreationBDn3 IS DATE;
statutNiveauBDn3 IS VARCHAR2(20);
delaiApparitionMinimumBDn3 IS NUMBER;
delaiApparitionMaximumBDn3 IS NUMBER;
tank1PosXBDn3 IS NUMBER;
tank1PosYBDn3 IS NUMBER;
tank2PosXBDn3 IS NUMBER;
tank2PosYBDn3 IS NUMBER;

/*INFO BD matrice1 de niveau3
id_niveauBDn3 IS NUMBER; /*fk
col1BDn3 IS NUMBER;
col2BDn3 IS NUMBER;
col3BDn3 IS NUMBER;
col4BDn3 IS NUMBER;
col5BDn3 IS NUMBER;
col6BDn3 IS NUMBER;
col7BDn3 IS NUMBER;
col8BDn3 IS NUMBER;
col9BDn3 IS NUMBER;
col10BDn3 IS NUMBER;
col11BDn3 IS NUMBER;
col12BDn3 IS NUMBER;

/*INFO BD niveau4
nomNiveauBDn4 IS VARCHAR2(20);
dateCreationBDn4 IS DATE;
statutNiveauBDn4 IS VARCHAR2(20);
delaiApparitionMinimumBDn4 IS NUMBER;
delaiApparitionMaximumBDn4 IS NUMBER;
tank1PosXBDn4 IS NUMBER;
tank1PosYBDn4 IS NUMBER;
tank2PosXBDn4 IS NUMBER;
tank2PosYBDn4 IS NUMBER;

/*INFO BD matrice1 de niveau4
id_niveauBDn4 IS NUMBER; /*fk
col1BDn4 IS NUMBER;
col2BDn4 IS NUMBER;
col3BDn4 IS NUMBER;
col4BDn4 IS NUMBER;
col5BDn4 IS NUMBER;
col6BDn4 IS NUMBER;
col7BDn4 IS NUMBER;
col8BDn4 IS NUMBER;
col9BDn4 IS NUMBER;
col10BDn4 IS NUMBER;
col11BDn4 IS NUMBER;
col12BDn4 IS NUMBER;

/*INFO BD niveau5
nomNiveauBDn5 IS VARCHAR2(20);
dateCreationBDn5 IS DATE;
statutNiveauBDn5 IS VARCHAR2(20);
delaiApparitionMinimumBDn5 IS NUMBER;
delaiApparitionMaximumBDn5 IS NUMBER;
tank1PosXBDn5 IS NUMBER;
tank1PosYBDn5 IS NUMBER;
tank2PosXBDn5 IS NUMBER;
tank2PosYBDn5 IS NUMBER;

/*INFO BD matrice1 de niveau5
id_niveauBDn5 IS NUMBER; /*fk
col1BDn5 IS NUMBER;
col2BDn5 IS NUMBER;
col3BDn5 IS NUMBER;
col4BDn5 IS NUMBER;
col5BDn5 IS NUMBER;
col6BDn5 IS NUMBER;
col7BDn5 IS NUMBER;
col8BDn5 IS NUMBER;
col9BDn5 IS NUMBER;
col10BDn5 IS NUMBER;
col11BDn5 IS NUMBER;
col12BDn5 IS NUMBER;
*/

CREATE OR REPLACE PROCEDURE creerNiveau(nomJoueur1 IN VARCHAR2, choixNiveau IN INTEGER)
AS
BEGIN
nomJoueurBD := SELECT nomJoueur FROM utilisateur [WHERE nomJoueur=nomJoueur1];
choixNiveauBD := SELECT nomNiveau FROM niveau [WHERE id=choixNiveau];

/*INFO BD niveau1*/
nomNiveauBDn1 := 'CardinVille';
dateCreationBDn1 := SYSDATE;
statutNiveauBDn1 := 'Public';
delaiApparitionMinimumBDn1 := 1;
delaiApparitionMaximumBDn1 := 2;
tank1PosXBDn1 := 2;
tank1PosYBDn1 := 2;
tank2PosXBDn1 := 5;
tank2PosYBDn1 := 5;

/*INFO BD matrice1 de niveau1*/
id_niveauBDn1 := SELECT id FROM niveau [WHERE = ]
col1BDn1
col2BDn1
col3BDn1
col4BDn1
col5BDn1
col6BDn1
col7BDn1
col8BDn1
col9BDn1
col10BDn1
col11BDn1
col12BDn1

/*INFO BD niveau2*/
nomNiveauBDn2 := 'Cardin';
dateCreationBDn2 := SYSDATE;
statutNiveauBDn2 := ''
delaiApparitionMinimumBDn2 := 
delaiApparitionMaximumBDn2 :=
tank1PosXBDn2 :=
tank1PosYBDn2 :=
tank2PosXBDn2 :=
tank2PosYBDn2 :=

id_niveauBDn2 :=
col1BDn2 :=
col2BDn2 :=
col3BDn2 :=
col4BDn2 :=
col5BDn2 :=
col6BDn2 :=
col7BDn2 :=
col8BDn2 :=
col9BDn2 :=
col10BDn2 :=
col11BDn2 :=
col12BDn2 :=

	IF nomJoueurBD AND choixNiveauBD != null THEN
	{
		dbms_output.put_line(CONCAT('Le joueur : ', nomJoueur, 'a creer/choisi le niveau');
	}
	ELSE
	{
		dbms_output.put_line('ERREUR avec votre query');
	}
	END IF;
END;