# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import messagebox
from tkinter.font import BOLD
from tkinter import font
import tkinter.ttk as ttk
from tkinter.colorchooser import *
import webbrowser
import re
import random
import time


######### RICHARD ##########
import dependancies
import dtoUser
import DAOOracleUser
############################


class Vue():
	def __init__(self,parent): 
		self.root=Tk()  
		self.root.title("Application de consultation")    
		self.root.maxsize(1200,750)
		self.root.minsize(1200,750)
		photo = PhotoImage(file="image.gif")
		l = Label(self.root,image=photo)
		l.photo = photo
		l.place(in_=self.root)
		self.couleurbg = "dark olive green"
		self.bChangerCouleurTank=None
		self.fSignUp=None
		self.canevas = None

		self.paddingX = 10
		self.paddingY = 5
		self.parent = parent
		self.currentFrame = None
		self.fontLabel = font.Font(family="Century Gothic", size=14,weight="bold")
		self.fontEntry = font.Font(family="Century Gothic", size=14)
		self.fontBouton = font.Font(family="Century Gothic", size=14,weight="bold")
		self.afficherPagePrincipale()

	def showView(self,fenetre):
		if self.currentFrame:
			self.currentFrame.pack_forget()
		if fenetre == "MenuPrincipale" :
			self.fMenuPrincipaleCentre.pack(expand=Y)
			self.currentFrame = self.fMenuPrincipaleCentre
		elif fenetre == "MenuSignUp":
			self.fSignUpCentre.pack(expand=Y)
			self.currentFrame = self.fSignUpCentre
		elif fenetre == "MenuMDPOublier":
			self.fMDPOublierCentre.pack(expand=Y)
			self.currentFrame = self.fMDPOublierCentre
		elif fenetre=="PagePersonnelle":
			self.fPagePersonnelleCentre.pack(expand=Y)
			self.currentFrame = self.fPagePersonnelleCentre
		elif fenetre=="PagePublique":
			self.fPagePubliqueCentre.pack(expand=Y)
			self.currentFrame=self.fPagePubliqueCentre
		elif fenetre=="PagePublique2":
			self.fPagePubliqueCentre2.pack(expand=Y)
			self.currentFrame=self.fPagePubliqueCentre2
		elif fenetre=="PagePartieInfo":
			self.fPagePartieInfoCentre.pack(expand=Y)
			self.currentFrame = self.fPagePartieInfoCentre

	def paddingConfigure(self, frame):
		frame.config(bg=self.couleurbg)

		for widget in frame.winfo_children():
			if frame != self.fSignUp:
				widget.grid_configure(pady=self.paddingY,padx=self.paddingY)
			if widget.winfo_class() == "Button" or widget.winfo_class() == "Label":
				if widget.winfo_class() == "Button":
					widget.config(font=self.fontBouton,fg="white")
					if widget != self.bChangerCouleurTank:
						widget.config(bg="dark green")
				else:
					widget.config(bg=self.couleurbg,fg="white")

	def envoyerEmail(self,recipients, subject, body):
		webbrowser.open("mailto:%s?subject=%s&body=%s" %(recipients, subject, body))

	def afficherPagePrincipale(self):
		self.parent.effacerDTOUser()

		rowMtn = 0
		self.fMenuPrincipaleCentre=Frame(self.root)
		self.fMenuPrincipale = Frame(self.fMenuPrincipaleCentre,width=500,height=500,bg=self.couleurbg)
		self.fMenuPrincipale.pack()
		self.showView("MenuPrincipale")


		rowMtn+=1
		lUsername = Label(self.fMenuPrincipale,text="Username: ", font=self.fontLabel,bg=self.couleurbg)
		lUsername.grid(row=rowMtn,column=0)
		rowMtn+=1

		eUsername = Entry(self.fMenuPrincipale, font=self.fontEntry)
		eUsername.grid(row=rowMtn)
		eUsername.focus()

		rowMtn += 1
		lMDP = Label(self.fMenuPrincipale,text="Mot de passe: ", font=self.fontLabel,bg=self.couleurbg)
		lMDP.grid(row=rowMtn,column=0)
		rowMtn+=1

		eMDP = Entry(self.fMenuPrincipale,show="*", font=self.fontEntry)
		eMDP.grid(row=rowMtn)

		rowMtn+=1

		bSignIn = Button(self.fMenuPrincipale,text="Sign In",command=lambda: self.signIn(eUsername.get(),eMDP.get()), font=self.fontBouton ,bg=self.couleurbg)
		bSignIn.grid(row=rowMtn)
		rowMtn += 1
		bMDPOublier = Button(self.fMenuPrincipale,text="Mot de passe oublie?",command=self.afficherMdpOublier1, font=self.fontBouton,bg=self.couleurbg )
		bMDPOublier.grid(row=rowMtn)
		rowMtn+=1

		lNouveauUtilisateur = Label(self.fMenuPrincipale,text="Nouvel Utilisateur?", font=self.fontLabel,bg=self.couleurbg)
		lNouveauUtilisateur.grid(row=rowMtn,column=0)
		rowMtn+=1

		bSignUp = Button(self.fMenuPrincipale,text="Sign Up", command=self.afficherSignUp, font=self.fontBouton,bg=self.couleurbg )
		bSignUp.grid(row=rowMtn)
		rowMtn+=1

		bSupport = Button(self.fMenuPrincipale,text="Support", command=self.support, font=self.fontBouton,bg=self.couleurbg )
		bSupport.grid(row=rowMtn)
		rowMtn+=1

		lRechercher = Label(self.fMenuPrincipale,font=self.fontLabel,text="Rechercher un joueur").grid(row=rowMtn)

		rowMtn+=1

		eRechercher = Entry(self.fMenuPrincipale,font=self.fontEntry)
		eRechercher.grid(row=rowMtn)
		rowMtn+=1
		bRechercher = Button(self.fMenuPrincipale,font=self.fontBouton,text="Rechercher", command=lambda:self.bRechercherJoueur(eRechercher.get()))

		self.paddingConfigure(self.fMenuPrincipale)

	def afficherMdpOublier1(self):
		rowMtn = 0
		self.fMDPOublierCentre = Frame(self.root)
		self.fMDPOublier = Frame(self.fMDPOublierCentre)
		self.fMDPOublier.pack()
		self.showView("MenuMDPOublier")

		lNomJoueur = Label(self.fMDPOublier,text="NomJoueur: ", font=self.fontLabel)
		lNomJoueur.grid(row=rowMtn)
		rowMtn+=1
		eNomJoueur = Entry(self.fMDPOublier, font=self.fontEntry)
		eNomJoueur.grid(row=rowMtn)
		eNomJoueur.focus()

		rowMtn += 1
		bContinuer = Button(self.fMDPOublier,text="Continuer",font=self.fontBouton,command=lambda:self.verifierJoueurExiste(eNomJoueur.get())).grid(row=rowMtn)
		rowMtn +=1
		bRetour = Button(self.fMDPOublier,text="Retourner", font=self.fontBouton,command=self.afficherPagePrincipale).grid(row=rowMtn)
		self.paddingConfigure(self.fMDPOublier)

	def verifierJoueurExiste(self,nomJoueur):
		if nomJoueur:
			if not self.parent.verifierNomJoueurUnique(nomJoueur): # l'usager existe
				if self.parent.getInfoJoueur(nomJoueur): # setter dans le controleur le dto utilise
					self.afficherMdpOublier2()
			else: #l'usager n'existe pas
				self.popperFenetreMessage("Erreur",nomJoueur+" n'existe pas.")
		else:
			self.popperFenetreMessage("Erreur","Veuillez remplir tous les champs.")

	def afficherMdpOublier2(self):
		rowMtn = 0
		self.fMDPOublierCentre = Frame(self.root)
		self.fMDPOublier = Frame(self.fMDPOublierCentre)
		self.fMDPOublier.pack()
		self.showView("MenuMDPOublier")

		dtoUser = self.parent.getDTOUser() # prendre le dto qui est setter dnas le controleur

		lNomJoueur = Label(self.fMDPOublier,text="Bonjour "+dtoUser.getNomJoueur(),font=self.fontLabel).grid(row=rowMtn)
		rowMtn+=1

		lQuestionA = Label(self.fMDPOublier,text=dtoUser.getQuestionSecreteA(), font=self.fontLabel)
		lQuestionA.grid(row=rowMtn)
		rowMtn+=1
		eRepQuestionA = Entry(self.fMDPOublier, font=self.fontEntry)
		eRepQuestionA.grid(row=rowMtn)
		eRepQuestionA.focus()
		rowMtn+=1

		lQuestionB = Label(self.fMDPOublier,text=dtoUser.getQuestionSecreteB(), font=self.fontLabel)
		lQuestionB.grid(row=rowMtn)
		rowMtn+=1
		eRepQuestionB = Entry(self.fMDPOublier, font=self.fontEntry)
		eRepQuestionB.grid(row=rowMtn)
		rowMtn+=1
		bContinuer = Button(self.fMDPOublier,text="Continuer",font=self.fontBouton,command=lambda:self.verifierQuestionMatch(eRepQuestionA.get(),eRepQuestionB.get()))
		bContinuer.grid(row=rowMtn)
		rowMtn +=1
		bRetour = Button(self.fMDPOublier,text="Retourner", font=self.fontBouton,command=self.afficherPagePrincipale).grid(row=rowMtn)
		self.paddingConfigure(self.fMDPOublier)

	def support(self):
		self.envoyerEmail("LaPimptresseEtSesTroisGigolos@tankem.com","Tech Support","Hello tankem,")


	def verifierQuestionMatch(self,repA,repB):
		dtoUser = self.parent.getDTOUser() # prendre le dto qui est setter dnas le controleur

		if self.verifierSiCaseVide([repA,repB]):
			if repA == dtoUser.getReponseA() and repB == dtoUser.getReponseB(): #les reponses concordent
				self.afficherMdpOublier3() 
			else:				
				self.popperFenetreMessage("Erreur", "Les reponses ne concordent pas.")

	def afficherMdpOublier3(self):
		rowMtn = 0
		self.fMDPOublierCentre = Frame(self.root)
		self.fMDPOublier = Frame(self.fMDPOublierCentre)
		self.fMDPOublier.pack()
		self.showView("MenuMDPOublier")
		dtoUser = self.parent.getDTOUser() # prendre le dto qui est setter dnas le controleur

		lNomJoueur = Label(self.fMDPOublier,text="Bonjour "+dtoUser.getNomJoueur(),font=self.fontLabel).grid(row=rowMtn)

		rowMtn+=1
		lMDP = Label(self.fMDPOublier,text="Mot de passe: ", font=self.fontLabel)
		lMDP.grid(row=rowMtn)
		rowMtn+=1
		eMDP = Entry(self.fMDPOublier,show="*", font=self.fontEntry)
		eMDP.grid(row=rowMtn)
		rowMtn+=1

		lRepeterMDP = Label(self.fMDPOublier,text="Repeter le mot de passe: ", font=self.fontLabel)
		lRepeterMDP.grid(row=rowMtn)
		rowMtn+=1
		eRepeterMDP = Entry(self.fMDPOublier,show="*", font=self.fontEntry)
		eRepeterMDP.grid(row=rowMtn)
		rowMtn+=1

		bChanger = Button(self.fMDPOublier,text="Changer", font=self.fontBouton,command=lambda:self.mdpOublier(eMDP.get(),eRepeterMDP.get())).grid(row=rowMtn)
		rowMtn+=1
		bRetour = Button(self.fMDPOublier,text="Retourner", font=self.fontBouton,command=self.afficherPagePrincipale).grid(row=rowMtn)
		self.paddingConfigure(self.fMDPOublier)
	
	def afficherSignUp(self):
		rowMtn = 0
		self.fSignUpCentre = Frame(self.root)
		self.fSignUp = Frame(self.fSignUpCentre)
		self.fSignUp.pack()
		self.showView("MenuSignUp")

		lNom = Label(self.fSignUp,text="Nom: ", font=self.fontLabel)
		lNom.grid(row=rowMtn,column=0)
		rowMtn+=1

		eNom = Entry(self.fSignUp, font=self.fontEntry)
		eNom.grid(row=rowMtn)
		eNom.focus()
		rowMtn+=1

		lPrenom = Label(self.fSignUp,text="Prenom: ", font=self.fontLabel)
		lPrenom.grid(row=rowMtn,column=0)
		rowMtn+=1

		ePrenom = Entry(self.fSignUp, font=self.fontEntry)
		ePrenom.grid(row=rowMtn)
		rowMtn+=1

		lNomJoueur = Label(self.fSignUp,text="NomJoueur: ", font=self.fontLabel)
		lNomJoueur.grid(row=rowMtn,column=0)
		rowMtn+=1

		eNomJoueur = Entry(self.fSignUp, font=self.fontEntry)
		eNomJoueur.grid(row=rowMtn)
		rowMtn+=1

		lMDP = Label(self.fSignUp,text="Mot de passe: ", font=self.fontLabel)
		lMDP.grid(row=rowMtn,column=0)
		rowMtn+=1

		eMDP = Entry(self.fSignUp,show="*", font=self.fontEntry)
		eMDP.grid(row=rowMtn)
		rowMtn+=1

		lRepeterMDP = Label(self.fSignUp,text="Repeter le mot de passe: ", font=self.fontLabel)
		lRepeterMDP.grid(row=rowMtn,column=0)
		rowMtn+=1

		eRepeterMDP = Entry(self.fSignUp,show="*", font=self.fontEntry)
		eRepeterMDP.grid(row=rowMtn)
		rowMtn+=1

		lQuestionA = Label(self.fSignUp,text="Question secrete A: ", font=self.fontLabel)
		lQuestionA.grid(row=rowMtn,column=0)
		rowMtn+=1

		eQuestionA = Entry(self.fSignUp, font=self.fontEntry)
		eQuestionA.grid(row=rowMtn)
		rowMtn+=1

		lRepQuestionA = Label(self.fSignUp,text="Reponse a la question secrete A: ", font=self.fontLabel)
		lRepQuestionA.grid(row=rowMtn,column=0)
		rowMtn+=1

		eRepQuestionA = Entry(self.fSignUp, font=self.fontEntry)
		eRepQuestionA.grid(row=rowMtn)
		rowMtn+=1

		lQuestionB = Label(self.fSignUp,text="Question secrete B: ", font=self.fontLabel)
		lQuestionB.grid(row=rowMtn,column=0)
		rowMtn+=1

		eQuestionB = Entry(self.fSignUp, font=self.fontEntry)
		eQuestionB.grid(row=rowMtn)
		rowMtn+=1

		lRepQuestionB = Label(self.fSignUp,text="Reponse a la question secrete B: ", font=self.fontLabel)
		lRepQuestionB.grid(row=rowMtn,column=0)
		rowMtn+=1

		eRepQuestionB = Entry(self.fSignUp, font=self.fontEntry)
		eRepQuestionB.grid(row=rowMtn)
		rowMtn+=1

		bInscrire = Button(self.fSignUp,text="S'Inscrire", font=self.fontBouton ,command=lambda:self.inscrire(eNom.get(),ePrenom.get(),eNomJoueur.get(),eMDP.get(),eRepeterMDP.get(),eQuestionA.get(),eRepQuestionA.get(), eQuestionB.get(), eRepQuestionB.get()) ).grid(row=rowMtn,pady=self.paddingY)
		rowMtn +=1

		bRetour = Button(self.fSignUp,text="Retourner", font=self.fontBouton,command=self.afficherPagePrincipale).grid(row=rowMtn,pady=self.paddingY)
		self.paddingConfigure(self.fSignUp)

	def afficherPagePersonnelle(self):
		rowMtn = 0
		self.fPagePersonnelleCentre = Frame(self.root)
		self.fPagePersonnelle = Frame(self.fPagePersonnelleCentre)
		self.fPagePersonnelle.pack()
		self.showView("PagePersonnelle")
		dtoUser = self.parent.getDTOUser() # on get le dto

		Label(self.fPagePersonnelle,text="Bonjour "+dtoUser.getPrenom()+" "+dtoUser.getNom(),font=self.fontLabel).grid(row=rowMtn)
		Button(self.fPagePersonnelle,text="Deconnecter",font=self.fontBouton,command=self.afficherPagePrincipale).grid(row=rowMtn,column=1)
		Button(self.fPagePersonnelle,text="Changer mot de pase",font=self.fontBouton,command=self.afficherMdpOublier1).grid(row=rowMtn,column=2)
		rowMtn+=1
		Label(self.fPagePersonnelle,text=dtoUser.getNomJoueur(), font=self.fontLabel).grid(row=rowMtn)
		Button(self.fPagePersonnelle,text="Voir ma page publique",command=self.afficherPagePublique).grid(row=rowMtn,column=2)
		rowMtn+=1
		Label(self.fPagePersonnelle,text="Niveaux favoris",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePersonnelle,text="Niveaux cree",font=self.fontLabel).grid(row=rowMtn,column=1)
		Label(self.fPagePersonnelle,text="Armurerie",font=self.fontLabel).grid(row=rowMtn,column=2)
		rowMtn+=1

		self.treeNiveauFavori = ttk.Treeview(self.fPagePersonnelle)
		self.treeNiveauFavori.heading('#0',text="Nom des niveaux")
		self.treeNiveauFavori.grid(column=0, row=rowMtn)

		self.treeNiveauCree = ttk.Treeview(self.fPagePersonnelle)
		self.treeNiveauCree.heading('#0',text="Nom des niveaux")
#		self.treeNiveauCree = ttk.Treeview(self.fPagePersonnelle,show="headings")
#		self.treeNiveauCree["columns"]=("one","two","three")
#		self.treeNiveauCree.column("three",width=5)
#		self.treeNiveauCree.column("two",width=5)
#		self.treeNiveauCree.heading('one',text="Niveaux")
#		self.treeNiveauCree.heading('two',text="Nbre Favoris")
#		self.treeNiveauCree.heading('three',text="Nbre joues")
		self.treeNiveauCree.grid(column=1, row=rowMtn)

		self.treeArmurerie = ttk.Treeview(self.fPagePersonnelle,show="headings")
		self.treeArmurerie["columns"]=("one","two")
		self.treeArmurerie.column("two",width=5)
		self.treeArmurerie.heading('one',text="Armes")
		self.treeArmurerie.heading('two',text="Quantite")
		self.treeArmurerie.grid(column=2, row=rowMtn)

		# AJOUTER LES VALEURS DANS LES TABLES (NIVEAU CREES, NIVEAUX FAVORIS, ARMURERIE)
		for i in dtoUser.getListeNiveauxCrees():
			self.ajouterLigneDansTree(self.treeNiveauCree,[i])

		for i in dtoUser.getListeNiveauxFavoris():
			self.ajouterLigneDansTree(self.treeNiveauFavori,[i])

		dictArme = dtoUser.getListeArme()
		for i in dictArme:
			listeValeur	= []
			listeValeur.append(i)
			listeValeur.append(dictArme[i])
			self.ajouterLigneDansTree(self.treeArmurerie,listeValeur)

		rowMtn+=1
		self.bChangerCouleurTank = Button(self.fPagePersonnelle,bg=dtoUser.getColor(), text='Changer la couleur', font=self.fontLabel, command=self.changerCouleurTank)
		self.bChangerCouleurTank.grid(row=rowMtn)

		bSauvegarder = Button(self.fPagePersonnelle,text="Sauvegarder",command=self.parent.saveTankCouleur).grid(row=rowMtn,column=1)
		#rowMtn+=1
		self.canevasTank = Canvas(self.fPagePersonnelle,width=150,height=110,bg="white")
		self.canevasTank.grid(row=rowMtn,column=2)
		self.tank = self.canevasTank.create_polygon(50,10,80,10,80,40,110,40,110,90,150,90,130,110,30,110,10,90,40,90,40,60,0,60,0,40,50,40,50,10,fill=dtoUser.getColor())
		
		# pour l'esthetique
		self.paddingConfigure(self.fPagePersonnelle)

	def changerCouleurTank(self):
		color = askcolor() # ouvre la palette de couleur et retourne la couleur choisi
		if color:
			self.bChangerCouleurTank.config(bg=color[1])			# change la couleur du bouton
			self.canevasTank.itemconfig(self.tank,fill=color[1])	# change la couleur du tank

		# garder en memoire temporairement la couleur choisi
		self.parent.setCouleurTank(color)

	def ajouterLigneDansTree(self, treeSelect, ligne):
		if len(ligne) > 1:
			treeSelect.insert('','end',values=ligne)
		else:
			treeSelect.insert('','end',text=ligne[0])

	def verifierSiCaseVide(self,listeDonnees):
		remplirChamps = True
		for i in listeDonnees:
			if not i:
				remplirChamps = False

		if not remplirChamps:
			self.popperFenetreMessage("Erreur","Veuillez remplir tous les champs")
		return remplirChamps


	def verifierMDP(self,mdp,repeterMDP):
		message=""
		# VERIFIER SI LES MOTS DE PASSES SE CONCORDENT
		if mdp != repeterMDP: # si les mdp ne concordent pas
			message = " Tes mots de passes ne concordent pas!"
		
		# VERIFIE SI LE MOT DE PASSE POSSEDE LES CRITERES
		elif not (any(x.isupper() for x in mdp) and any(x.islower() for x in mdp) and any(x.isdigit() for x in mdp) and len(mdp) >= 9 and not re.match(r'[!@#$%?&*()]',mdp)):
			message="Votre mot de passe doit contenir au minimum 9 lettres.\nAu moins une letttre minuscule.\nAu moins une lettre majuscule.\nAu moins un chiffre.\nAu moins un symbole. Supporter au moins ceux-ci: !@#$%?&*()"
		
		return message

	def popperFenetreMessage(self,titre,message):
		# pop une fenetre pour confirmer une action
		reponse=messagebox.askokcancel(titre,message)
		if reponse:
			return True
		else:
			return False

	def genererRandomCouleur(self):
		color = "#%06x" % random.randint(0, 0xFFFFFF)
		return color

	def inscrire(self,nom,prenom,nomJoueur,mdp,repeterMDP,questionA,reponseA,questionB,reponseB):
		listeInscrire = [nom,prenom,nomJoueur,mdp,repeterMDP,questionA,reponseA,questionB,reponseB]

		# verifier si les champs sont rempli
		remplirChamps = self.verifierSiCaseVide(listeInscrire)

		if remplirChamps: # si les champs sont remplis
			message = self.verifierMDP(mdp,repeterMDP) # verifie si le mdp respectent tous les demandes
			# VERIFIER SI LE NOM DE JOUEUR EXISTE DEJA

			if not self.parent.verifierNomJoueurUnique(nomJoueur):
				self.popperFenetreMessage("Erreur","Le nom de joueur "+nomJoueur+" existe deja. Veuillez le changer!")
			elif message:
				self.popperFenetreMessage("Erreur",message)
			# CREER L'UTILISATEUR
			elif self.parent.inscrire(nom,prenom,nomJoueur,mdp,questionA,reponseA,questionB,reponseB,self.genererRandomCouleur()):
				self.popperFenetreMessage("Inscription succes","Votre compte a ete cree!")
				self.afficherPagePrincipale()

	def mdpOublier(self,mdp,repeterMDP):
		dtoUser = self.parent.getDTOUser() # on get le dto
		nomJoueur = dtoUser.getNomJoueur()
		message = self.verifierMDP(mdp,repeterMDP)
		if self.verifierSiCaseVide([nomJoueur,mdp,repeterMDP]):
			if mdp == repeterMDP:
				if message:
					self.popperFenetreMessage("Erreur",message)
				else:
					if self.parent.resetMDP(dtoUser.getNomJoueur(),mdp):
						self.popperFenetreMessage("Mot de passe modifie","Votre mot de passe a ete modifie avec succes.")
						self.afficherPagePrincipale() # retourne dans la page principale
			else:
				self.popperFenetreMessage("Erreur","Les mots de passe ne concordent pas")

	def signIn(self,username,mdp):
		listeSignIn = [username,mdp]
		if self.verifierSiCaseVide(listeSignIn):
			if self.parent.signIn(username,mdp): # si ce sont les bonnes coordonnees
				self.afficherPagePersonnelle()
			else:
				self.popperFenetreMessage("Erreur","Les champs entrees sont incorrecte.")

###########################################################################################################################################################
################################################################ TANKEM PHASE 4 ###########################################################################
###########################################################################################################################################################


	def bRechercherJoueur(self,nomJoueur):
		if not nomJoueur:
			self.popperFenetreMessage("Erreur","Veuillez remplir tous les champs.")
		else:
			if self.parent.getInfoJoueur(nomJoueur):
				self.afficherPagePublique()
			else:
				self.popperFenetreMessage("Erreur","Cet utilisateur n'existe pas!")


	def afficherPagePublique(self):
		rowMtn = 0
		self.fPagePubliqueCentre = Frame(self.root)
		self.fPagePublique = Frame(self.fPagePubliqueCentre)
		self.fPagePublique.pack()
		self.showView("PagePublique")

		dtoUser = self.parent.getDTOUser() # on get le dto
		#DTOStatistique = Nom du fichier/class
		#dtoStat = variable

		Label(self.fPagePublique,text="La page publique de "+dtoUser.getNomJoueur(), font=self.fontLabel).grid(row=rowMtn)

		rowMtn+=1


		Label(self.fPagePublique, text="Nom de reputation",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="self.parent.getNomReputation()",font=self.fontLabel).grid(row=rowMtn,column=3)


		rowMtn+=1
		Label(self.fPagePublique,text="Taux gagnant/perdant",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="self.parent.getTauxGagnantPerdant()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Taux abandon/total",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="self.parent.getTauxAbandonTotal()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Parties gagnées",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="dtoStat.getNbPartieGagnees()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Parties perdues",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="dtoStat.getNbPartiePerdues()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Parties abandonnees",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="dtoStat.getNbPartieAbandonnees()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Total parties",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="dtoStat.getNbTotalParties()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Nombre total de niveaux crees",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="dtoStat.getTotalNiveauxCrees()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Arme utilise frequemment",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="dtoStat.getArmeFavori()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Dommage moyen/arme",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="self.parent.getDommageMoyenArme()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1
		Label(self.fPagePublique,text="Dommage moyen",font=self.fontLabel).grid(row=rowMtn)
		Label(self.fPagePublique, text="self.parent.getDommageMoyer()",font=self.fontLabel).grid(row=rowMtn,column=3)

		rowMtn+=1

		bVoirPageSuivant = Button(self.fPagePublique,font=self.fontBouton,text="Voir les parties joues",command=self.afficherPagePublique2)
		bVoirPageSuivant.grid(row=rowMtn,column=3)
		bRetour = Button(self.fPagePublique,font=self.fontBouton,text="Retourner au menu principale",command=self.afficherPagePrincipale)
		bRetour.grid(row=rowMtn)
		rowMtn+=1

		self.paddingConfigure(self.fPagePublique)

	def afficherPagePublique2(self):
		rowMtn = 0
		self.fPagePubliqueCentre2 = Frame(self.root)
		self.fPagePublique2 = Frame(self.fPagePubliqueCentre2)
		self.fPagePublique2.pack()
		self.showView("PagePublique2")

		dtoUser = self.parent.getDTOUser() # on get le dto
		#DTOStatistique = Nom du fichier/class
		#dtoStat = variable
		Label(self.fPagePublique2,font=self.fontLabel,text="Les parties jouees par "+dtoUser.getNomJoueur()).grid(row=rowMtn,column=0,columnspan=2)
		rowMtn+=1
		self.lbPartieJoue = Listbox(self.fPagePublique2,font=self.fontEntry,selectmode=SINGLE)
		self.lbPartieJoue.grid(row=rowMtn)
		self.lbPartieJoue.insert(END,"TRATRAT")
		self.lbPartieJoue.insert(END,"tra2")

		bVisualiser = Button(self.fPagePublique2,font=self.fontBouton,text="Visualisez partie",command=self.bVisualiserPartie)
		bVisualiser.grid(row=rowMtn,column=3)

		self.paddingConfigure(self.fPagePublique2)

	def bVisualiserPartie(self):
		index = self.lbPartieJoue.curselection()
		if index:
			jeuChoisi = self.lbPartieJoue.get(index[0])
			self.afficherPartieInfo(jeuChoisi)
		else:
			self.popperFenetreMessage("Erreur","Veuillez selectionner une partie!")

	def afficherPartieInfo(self,jeuChoisi):
		self.fPagePartieInfoCentre = Frame(self.root)
		self.showView("PagePartieInfo")

		self.fInfoGagnant = Frame(self.fPagePartieInfoCentre)
		self.fInfoGagnant.pack(side=TOP)

		row1=0
		Label(self.fInfoGagnant,text="Nom du niveau:",font=self.fontLabel).grid(row=row1)
		Label(self.fInfoGagnant,text=jeuChoisi,font=self.fontLabel).grid(row=row1,column=3)

		row1+=1
		Label(self.fInfoGagnant,text="Conclusion:",font=self.fontLabel).grid(row=row1)
		Label(self.fInfoGagnant,text="Joueur1 a gagne / Joueur2 a gagne / Partie nulle",font=self.fontLabel).grid(row=row1,column=3)

		row1+=1
		Label(self.fInfoGagnant,text="Mode HeatMap:",font=self.fontLabel).grid(row=row1)
		valueType = StringVar()
		self.cbModeHeatMap = ttk.Combobox(self.fInfoGagnant, textvariable=valueType, state='readonly')
		self.cbModeHeatMap['values'] = ('Temps total passe', 'Dommage prise','Dommage donne')
		self.cbModeHeatMap.current(0)
		self.cbModeHeatMap.grid(row=row1,column=3)

		row1+=1
		bAfficherHeatMap = Button(self.fInfoGagnant,text="Afficher",font=self.fontBouton,command=self.modeChoisiHM)
		bAfficherHeatMap.grid(row=row1,column=3)


		row1+=1
		Label(self.fInfoGagnant,text="Nom de gagnant",font=self.fontLabel).grid(row=row1)
		# REMARQUE: HARD CODER LE NOM DU JOUEUR EN S
		bNomReputationGagnant = Button(self.fInfoGagnant,text="self.parent.getNomReputation(gagnant)",font=self.fontBouton,bg="red",command=lambda:self.dirigerPagePublique("s"))
		bNomReputationGagnant.grid(row=row1,column=3)

		self.fHeatMap = Frame(self.fPagePartieInfoCentre)
		self.fHeatMap.pack(side=TOP)


		self.paddingConfigure(self.fHeatMap)

	def afficherHeatMap(self,matrice,legende):
		if self.canevas:
			self.canevas.pack_forget()
			print("exist")
		# heatmap
		debutX = 10
		debutY = 10
		distance = 30
		longueurCanvas = (distance * len(matrice[0])) + debutX + 200
		hauteurCanvas = (distance * len(matrice)) + debutY + debutY
		self.canevas = Canvas(self.fHeatMap,width=longueurCanvas,height=hauteurCanvas,bg=self.couleurbg)
		self.canevas.pack()
		for i in range(len(matrice)):
			for j in matrice[i]:
				debutX += distance
				finX = debutX + distance
				finY = debutY + distance
				color = '#%02x%02x%02x' % j # changer de rgb en HEXCODE
				self.canevas.create_rectangle(debutX,debutY,finX,finY,fill=color,outline="black")
			debutX = 10
			debutY += distance

		# LEGENDE

		initX = finX + 10
		debutX = initX
		debutY = 50
		distance = 20
		self.canevas.create_text(initX+60,30,text="Legende",font=self.fontEntry)
		for i in range(len(legende)):
			for j in legende[i]:
				debutX += distance
				finX = debutX + distance
				finY = debutY + distance
				color = '#%02x%02x%02x' % j # changer de rgb en HEXCODE
				self.canevas.create_rectangle(debutX,debutY,finX,finY,fill=color,outline=color)
			debutX = initX
			debutY += distance

	def dirigerPagePublique(self,nomJoueur):
		self.parent.getInfoJoueur(nomJoueur)
		self.afficherPagePublique()

	def modeChoisiHM(self):
		modechoisi = self.cbModeHeatMap.get()
		print(modechoisi)

		matLegende = [
			[(0,5),(0,4),(0,3),(0,2),(0,1),(0,0)],
			[(1,5),(1,4),(1,3),(1,2),(1,1),(1,0)],
			[(2,5),(2,4),(2,3),(2,2),(2,1),(2,0)],
			[(3,5),(3,4),(3,3),(3,2),(3,1),(3,0)],
			[(4,5),(4,4),(4,3),(4,2),(4,1),(4,0)],
			[(5,5),(5,4),(5,3),(5,2),(5,1),(5,0)]
		]


		matTuple = [ 
			[(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)]
		]

		matTuple2 = [ 
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(17,4),(11,9),(0,50),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)],
			[(8,17),(12,10),(9,9),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10),(15,2),(13,5),(5,10)]
		]

		if modechoisi == "Dommage prise":
			self.heatmap = HeatMap(matTuple)
		elif modechoisi == "Temps total passe":
			self.heatmap = HeatMap(matTuple2)
		matriceHM = self.heatmap.getMatriceCouleur()

		self.legende = HeatMap(matLegende)
		matriceLegende = self.legende.getMatriceCouleur()
		self.afficherHeatMap(matriceHM,matriceLegende)

class HeatMap():
	def __init__(self,matrice):
		self.matrice= matrice
		self.maximum = self.trouverMax()
		self.matricePourcentage = self.changerMatriceEnPourcentage()
		#self.changerMatriceEnCouleur()
		self.matriceCouleur = self.changerMatriceEnCouleur()

	def getMatriceCouleur(self):
		return self.matriceCouleur

	def trouverMax(self):
		listeMax = []
		for i in self.matrice:
			for j in i:
				listeMax.append(max(j))
		return max(listeMax)

	def changerMatriceEnPourcentage(self):
		nouvelleMatrice = []
		sousListe = []
		for i in self.matrice:
			sousListe = []
			for j in i:
				sousListe.append((j[0]/self.maximum,j[1]/self.maximum))
			nouvelleMatrice.append(sousListe)

		return nouvelleMatrice

	def changerMatriceEnCouleur(self):
		nouvelleMatrice = []
		sousListe = []
		couleurDataA = (255,0,0) #rouge
		couleurDataB = (128,0,128) #violet
		couleurDataAB = (0,0,255) #bleu
		couleurDepart = (255,255,255) #blanc
		len(self.matricePourcentage)
		for i in self.matricePourcentage:
			sousListe = []
			for j in i:
				fractionA = j[0]
				fractionB = j[1]
				couleurFinal = []

				for number in range(3):
					newColor1 = ((couleurDataA[number]-couleurDepart[number]) * fractionA) + couleurDepart[number]
					newColor2 = ((couleurDataB[number]-couleurDataAB[number])*fractionA)+ couleurDataAB[number]
					newColor = ((newColor2 - newColor1) * fractionB) + newColor1
					couleurFinal.append(newColor)

				sousListe.append(tuple(couleurFinal))
			nouvelleMatrice.append(sousListe)

		return nouvelleMatrice



class Controleur():
	def __init__(self):  
		self.tankCouleurtemp=None
		self.daoUser = DAOOracleUser.DAOOracleUser()
		self.dtoUser=None
		self.vue=Vue(self)
		self.vue.root.mainloop()

	def effacerDTOUser(self):
		self.dtoUser=None

	def signIn(self,nomJoueur,mdp):
		# Verifier dans la BD si les noms usagers et mdp correspondent
		# Si login success, return True
		# Si login failed, return False
		login = self.daoUser.authentifier(nomJoueur,mdp)
		if login == 2:
			self.vue.popperFenetreMessage("Erreur de connection","Desole, nous n'arrivons pas a connecter a la base de donnee.\nVeuillez reessayer plus tard.")			
		elif login:
			self.setInfoJoueur(self.getInfoJoueur(nomJoueur)) # si les donnees concordent,cherche tout son profil
			return True
		else:
			return False


	def inscrire(self,nom,prenom,nomJoueur,mdp,questionA,reponseA,questionB,reponseB,couleurTank):
		dto=dtoUser.DTOUser()

		dto.setAll(nom,prenom,nomJoueur,mdp,questionA,reponseA,questionB,reponseB,couleurTank,{},[],[])
		if not self.daoUser.createUser(dto):
			self.vue.popperFenetreMessage("Erreur de connection","Desole, nous n'arrivons pas a connecter a la base de donnee.\nVeuillez reessayer plus tard.")
		else:
			return True # ajouter user reussite


	def verifierNomJoueurUnique(self,nomJoueur):
		# Devrait verifier dans BD voir si le nomJoueur existe ou non
		# Si existe, return False
		# Si n'existe pas, return True
		unique = self.daoUser.getUser(nomJoueur)
		if unique == 2: # connection bd non etablie
			self.vue.popperFenetreMessage("Erreur de connection","Desole, nous n'arrivons pas a connecter a la base de donnee.\nVeuillez reessayer plus tard.")
		elif unique:
			return False #FALSE = LE NOM N'EST PAS UNIQUE
		else: # l'usager n'existe pas
			return True # TRUE = LE NOM EST UNIQUE

	def resetMDP(self,nomJoueur,mdp):
		# listeDonnees = [nomJoueur, mdp, repeterMDP]
		# Quand l'usager a oublier son mot de passe
		# on devrait verifier au bd si le nomJoueur,repQuestionA et repQuestionB correspondent
		self.dtoUser.setNomJoueur(nomJoueur)
		self.dtoUser.setPassword(mdp)
		misajour = self.daoUser.updateUser(self.dtoUser)
		if misajour == 2:
			self.vue.popperFenetreMessage("Erreur de connection","Desole, nous n'arrivons pas a connecter a la base de donnee.\nVeuillez reessayer plus tard.")
			return False # mis a jour failed
		elif misajour:
			return True #mis a jour reussite
		else:
			return False # mis a jour failed

	def getInfoJoueur(self,nomJoueur):
		dtoUser = self.daoUser.getUser(nomJoueur)
		#dtoStat = self.daoStat.getStat(nomJoueur)

		if dtoUser == 2:# or dtoStat == 2:
			self.vue.popperFenetreMessage("Erreur de connection","Desole, nous n'arrivons pas a connecter a la base de donnee.\nVeuillez reessayer plus tard.")
		else:
			#self.setInfoStatistique(dtoStat)
			self.setInfoJoueur(dtoUser)
			return dtoUser

	def setInfoStatistique(self, dto):
		self.dtoStat = dto

	def getDTOUser(self):
		# on va retourner le dto qui est setter dans le controleur
		return self.dtoUser

	def setInfoJoueur(self,dto):
		self.dtoUser = dto
		#print(self.dtoUser.getListeNiveauxCrees(),self.dtoUser.getListeNiveauxFavoris(),self.dtoUser.getListeArme())

	def setCouleurTank(self,couleur):
		self.tankCouleurtemp = couleur[1]

	def saveTankCouleur(self):
		# appeller bd pour save
		if (self.tankCouleurtemp==None):
			self.tankCouleurtemp=self.dtoUser.getColor()
		self.dtoUser.setColor(self.tankCouleurtemp)
		misajour = self.daoUser.updateUser(self.dtoUser)
		if misajour == 2:
			self.vue.popperFenetreMessage("Erreur de connection","Desole, nous n'arrivons pas a connecter a la base de donnee.\nVeuillez reessayer plus tard.")
		else:
			self.vue.popperFenetreMessage("Changement de couleur","Felicitation! Vous avez reussi a changer la couleur de votre tank.")
'''
	#####################
	# FONCTIONS CALCULS #
	#####################
	def getTauxGagnantPerdant(self):
		return self.dtoStat.getNbPartieGagnees / self.dtoStat.getNbPartiePerdues

	def getTauxAbandonTotal(self):
		return self.dtoStat.getNbPartieAbandonnees / self.dtoStat.getNbTotalParties

	def getDommageMoyenArme(self):
		# dict = { NomArme: [totalDommage, totaltir]}
		dictArme = self.dtoStat.getDictArme()
		dictDommageMoyen = {}
		for i in dictArme:
			totalDommage = dictArme[i][0]
			totalTir = dictArme[i][1]
			dictDommageMoyen[i] = totalDommage / totalTir

		return dictDommageMoyen

	def getDommageMoyenParTir(self):
		dictArme = self.dtoStat.getDictArme()
		totalDommage=0
		totalTir = 0		
		for i in dictArme:
			totalDommage += dictArme[i][0]
			totalTir += dictArme[i][1]

		return totalDommage / totalTir

	def getNomReputation(self):

		# CHERCHER LA QUALIFICATION A
		dictQualificationA = {
							"Piege":"Le subtil",
							"Guide":"Le hitman",
							"Mitraillette":"Le gangster",
							"Grenade":"L'exploseur de tete",
							"Shotgun":"Le dechiqueteur",
							"Spring":"La puce mexicaine"
		}
		qualificationA = ""
		qualificationB = ""
		armeFavori = self.dtoStat.getArmeFavori()
		partieGagne = self.dtoStat.getNbPartieGagnees() 
		if partieGagne < 2:
			qualificationA = "Le generaliste"
		else:
			qualificationA = dictQualificationA[armeFavori]


		# CHERCHER LA QUALIFICATION B
		dateLastJeu = self.dtoStat.getDateJoue() # get la date du dernier jeu joue
		dateToday = time.strftime("%Y/%m/%d") # Richard verifie le format
		partieTotal = dtoStat.getNbTotalParties¸()

		if self.getTauxAbandonTotal > 0.1:
			qualificationB = "poltron"
		elif dateToday - dateLastJeu > 7:
			qualificationB = "fantome"
		elif partieTotal < 5:
			qualificationB = "neophite"
		elif self.getTauxGagnantPerdant > 0.5:
			qualificationB = "vainqueur"

		return qualificationA + " " + qualificationB
'''


c = Controleur()