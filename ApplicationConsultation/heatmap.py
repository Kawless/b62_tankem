from tkinter import *


class Vue():
	def __init__(self,parent): 	
		self.root=Tk()  
		self.root.title("Application de consultation")    
		self.root.maxsize(800,680)
		self.root.minsize(800,680)

	def afficherHeatMap(self,matrice):
		self.canevas = Canvas(self.root,width=800,height=680,bg="white")
		self.canevas.pack()
		debutX = 10
		debutY = 10
		distance = 50
		for i in range(len(matrice)):
			for j in matrice[i]:
				print("j: ",j)
				debutX += distance
				finX = debutX + distance
				finY = debutY + distance
				color = '#%02x%02x%02x' % j # changer de rgb en HEXCODE
				self.canevas.create_rectangle(debutX,debutY,finX,finY,fill=color,outline="black")
			debutX = 10
			debutY += distance

	def afficherLegende(self,legende):
		self.cLegende = Canvas(self.root,width=200,height=200,bg="white")
		self.cLegende.pack()
		debutX = 10
		debutY = 10
		distance = 20
		for i in range(len(legende)):
			for j in legende[i]:
				print("j: ",j)
				debutX += distance
				finX = debutX + distance
				finY = debutY + distance
				color = '#%02x%02x%02x' % j # changer de rgb en HEXCODE
				self.cLegende.create_rectangle(debutX,debutY,finX,finY,fill=color,outline=color)
			debutX = 10
			debutY += distance


class HeatMap():
	def __init__(self,matrice):
		self.matrice= matrice
		self.maximum = self.trouverMax()
		self.matricePourcentage = self.changerMatriceEnPourcentage()
		#self.changerMatriceEnCouleur()
		self.matriceCouleur = self.changerMatriceEnCouleur()

	def getMatriceCouleur(self):
		return self.matriceCouleur

	def trouverMax(self):
		listeMax = []
		for i in self.matrice:
			for j in i:
				listeMax.append(max(j))
		return max(listeMax)

	def changerMatriceEnPourcentage(self):
		nouvelleMatrice = []
		sousListe = []
		for i in self.matrice:
			sousListe = []
			for j in i:
				sousListe.append((j[0]/self.maximum,j[1]/self.maximum))
			nouvelleMatrice.append(sousListe)

		return nouvelleMatrice

	def changerMatriceEnCouleur(self):
		nouvelleMatrice = []
		sousListe = []
		couleurDataA = (255,0,0) #rouge
		couleurDataB = (128,0,128) #violet
		couleurDataAB = (0,0,255) #bleu
		couleurDepart = (255,255,255) #blanc
		len(self.matricePourcentage)
		for i in self.matricePourcentage:
			sousListe = []
			for j in i:
				fractionA = j[0]
				fractionB = j[1]
				couleurFinal = []

				for number in range(3):
					newColor1 = ((couleurDataA[number]-couleurDepart[number]) * fractionA) + couleurDepart[number]
					#newColor2 = ((couleurDataB[number]-couleurDepart[number]) * fractionB) + couleurDepart[number]

					newColor2 = ((couleurDataB[number]-couleurDataAB[number])*fractionA)+ couleurDataAB[number]
					newColor = ((newColor2 - newColor1) * fractionB) + newColor1
					couleurFinal.append(newColor)

				sousListe.append(tuple(couleurFinal))
			nouvelleMatrice.append(sousListe)

		return nouvelleMatrice

class Controleur():
	def __init__(self):  
		self.vue=Vue(self)
		matTuple = [ 
			[(0,0),(0,0),(0,0)],
			[(17,4),(11,9),(0,50)],
			[(8,17),(12,10),(9,9)]
		]

		matLegende = [
			[(0,5),(0,4),(0,3),(0,2),(0,1),(0,0)],
			[(1,5),(1,4),(1,3),(1,2),(1,1),(1,0)],
			[(2,5),(2,4),(2,3),(2,2),(2,1),(2,0)],
			[(3,5),(3,4),(3,3),(3,2),(3,1),(3,0)],
			[(4,5),(4,4),(4,3),(4,2),(4,1),(4,0)],
			[(5,5),(5,4),(5,3),(5,2),(5,1),(5,0)]
		]

		self.legende = HeatMap(matLegende)
		self.vue.afficherLegende(self.legende.getMatriceCouleur())
		self.heatmap = HeatMap(matTuple)
		self.vue.afficherHeatMap(self.heatmap.getMatriceCouleur())
		self.vue.root.mainloop()


c = Controleur()