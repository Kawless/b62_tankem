READ ME ____ EDITEUR DE NIVEAU ____ LISEZ-MOI

1.D�marrage
2.Fonctionnement
3.Bug connus
4.� propos


_________________________________________________________________

1.D�MARRAGE DE L'�DITEUR

L'�diteur de niveau n�cessite l'utilisation de Python 3 (3.4).
l'interpr�teur python doit comporter le module tkinter. Un script cmd est �galement fourni pour d�marrer l'�diteur avec le chemin par d�fault de python (C:\Python34\python.exe). En autant que python s'y trouve, d�marrer l'application en double cliquant sur DemmarerEditeur.cmd.

_________________________________________________________________

2.FONCTIONNEMENT DE L'�DITEUR

L'�diteur peut �tre utilis� enti�rement avec le clavier ou conjointement avec la souris. Lors de l'ouverture du programme, le focus est automatiquement mis sur le premier champ et il s'afficher pa un carr� bleu. Pour changer de cible avec le focus, utiliser la touche TAB de votre clavier.

Afin de pouvoir bouger le cursor et modifier les cases, le focus doit �tre sur la grille (bien entendu, un carr� bleu vous indiquera assez clairement si le focus se trouve sur la grille ou non).

Touche de clavier:

TAB    -Changer le focus
WASD   -D�placer le curseur sur la grille
Fl�che -D�placer le curseur sur la grille / D�placer dans le menu
Espace -Cycler les tuiles
CTRL   -Cycler les tuiles inversement
SHIFT  -D�signer case comme d�part pour un tank
ALT    -Acc�der au menu
ENTER  -S�lectionner un �l�ment du menu

_________________________________________________________________

3.BUG CONNUS

Aucun bug n'a �t� trouv� avec l'utilisation du logiciel. Si vous en trouvez un, n'h�siter pas � soummettre un rapport de bug � notre �quipe technique afin que ce dernier puissent �tre corrig�.

_________________________________________________________________

4.� PROPOS

Application de cr�ation de niveau du futur "triple A game" Tankem cr�� par Richard Cardin et Nicolas Courcelles.

Aucun droit r�serv� - No.Copyright (c) 2016