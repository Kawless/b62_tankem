# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import font
from os import startfile
from tkinter import messagebox
import sys
sys.path.append('../Tankem/src') #Je n'arrivais pas a faire le import relatif, j'ajoute donc le path relatif au pythonpath. Cela permettra le import
from dto import DTONiveau
try:
	from dao import DAOOracleNiveau
except:
    pass
try:
	import dependancies
	import DAOOracleUser
	import dtoUser
except:
	pass
   
   



########################### VUE ###########################

class Vue():
    def __init__(self, parent, modele):
        self.parent=parent
        self.modele=modele
        self.tk=Tk()
        self.tk.title("Editeur de niveau")
        self.tk.config(width=900, height=600)
        self.tk.pack_propagate(False)
        self.cellSize=45 #Grosseur de chaque cellule à l'écran
        self.setupLogin()
        
    def setupLogin(self):
        self.frameLogin = Frame(self.tk)
        self.frameLogin.pack(expand=1)
        c="gray80"
        Label(self.frameLogin, text="Tankem Level Editor Login", font=("Algerian",36)).pack(pady=20)
        self.frameForm=Frame(self.frameLogin,border=3, relief=RAISED, bg=c)
        self.frameForm.pack(pady=20)
        Label(self.frameForm,text="Nom d'utilisateur : ",font=("Helvetica", 16),bg=c).grid(row=1,column=1, sticky=E,padx=20,pady=20)
        Label(self.frameForm,text="Mot de passe : ",font=("Helvetica", 16),bg=c).grid(row=3,column=1, sticky=E,padx=20,pady=20)
        self.entryName=Entry(self.frameForm, width=30,font=("Helvetica", 16))
        self.entryName.grid(row=1,column=2,padx=20)
        self.entryName.focus_set()
        self.entryPass=Entry(self.frameForm, width=30,font=("Helvetica", 16), show="*")
        self.entryPass.grid(row=3, column=2,padx=20)
        self.buttonConnect=Button(self.frameLogin, width=30, text="Connecter", font=("Helveltica", 16), bg=c,border=3, command=self.bConnect)
        self.buttonConnect.pack(pady=40)
        underlinedFont=font.Font(family='Helvetica', size=12, underline=True)
        self.labelPasswordForgotten=Label(self.frameLogin, text="Inscription / Support / Mot de passe oublié", fg="blue", font=underlinedFont)
        self.labelPasswordForgotten.pack()
        self.labelPasswordForgotten.bind("<Button-1>",self.support)
        self.entryName.bind("<Return>", self.connect)
        self.entryPass.bind("<Return>", self.connect)
    
    def connect(self, event):
    	self.bConnect()

    def bConnect(self):
    	try:
    		self.daoUsr=DAOOracleUser.DAOOracleUser()
    		result=self.daoUsr.authentifier(self.entryName.get(),self.entryPass.get())
    	except:
    		result=4
    	if(result==2):
    		self.afficherErreur("La connexion à la base de donnée a échoué")
    	elif(result==False):
    		self.afficherErreur("Le nom d'utilisateur ou le mot de passe est invalide")
    	elif(result==True):
    		self.parent.login(self.entryName.get())
    		print("Connecté en tant que "+self.entryName.get())
    		self.setupEditor()
    	elif(result==4):
    		messagebox.showinfo("Mode Anonyme","Problème d'authentification, démarrage de l'éditeur en mode anonyme.")
    		self.setupEditor()

	
    def support(self, event):
        print("done") #Redirigera vers l'application de gestion d'accompte

    def setupEditor(self):
        self.frameLogin.destroy()
        self.createFrames()
        self.setupMenuBar()
        self.setupLeftFrame()  #initialise le frame de configuration
        self.setupRightFrame() #initialise le frame de grille
        
    def createFrames(self):
        self.frameLeft = Frame(self.tk , width=300, height = 600)
        self.frameLeft.grid_propagate(FALSE)
        self.frameLeft.pack(side = LEFT)
        self.frameRight = Frame(self.tk , width=600, height = 600, bg = "dark gray")
        self.frameRight.pack_propagate(FALSE)
        self.frameRight.pack(side = LEFT)
        
    def setupMenuBar(self):
        self.menubar = Menu(self.tk)
        self.menuFichier = Menu(self.menubar, tearoff=0)
        self.menuFichier.add_command(label="Réinitialiser", command=self.resetEditor)
        self.menuFichier.add_command(label="Valider et Sauvegarder", command=self.sauvegarder)
        self.menuFichier.add_separator()
        self.menuFichier.add_command(label="Exit", command=self.tk.quit)
        self.menubar.add_cascade(label="Fichier", menu=self.menuFichier)
        
        self.menuAide = Menu(self.menubar, tearoff=0)
        self.menuAide.add_command(label="Contrainte de création de niveaux", command=self.afficherContrainte)
        self.menuAide.add_command(label="Afficher le fichier lisez-moi", command=self.afficherAide)
        self.menubar.add_cascade(label="Aide", menu=self.menuAide)
        
        self.tk.config(menu=self.menubar)
        
    def setupLeftFrame(self):
        #Titre et champs texte pour le titre
        Label(self.frameLeft).grid(row=2,column=2)
        Label(self.frameLeft, text="Titre du niveau :    ").grid(row=3,column=3)
        self.entryTitre = Entry(self.frameLeft,highlightthickness=2, highlightcolor="light blue1")
        self.entryTitre.grid(row=3, column=4)
        self.entryTitre.focus_set()
        
        #Controle de la largeur / Hauteur
        Label(self.frameLeft).grid(row=4,column=3)
        Label(self.frameLeft, text="Largeur :    ").grid(row=5, column=3)
        self.sbLargeur=Spinbox(self.frameLeft, from_ = 6, to = 12, width=8, state="readonly",highlightthickness=2, highlightcolor="light blue1", command=self.updateSize)
        self.sbLargeur.grid(row=5,column=4)
        Label(self.frameLeft, text="Hauteur :    ").grid(row=6, column=3)
        self.sbHauteur=Spinbox(self.frameLeft, from_ = 6, to = 12, width=8, state="readonly",highlightthickness=2, highlightcolor="light blue1", command=self.updateSize)
        self.sbHauteur.grid(row=6,column=4)
        
        #Controle du délai d'apparition des items
        Label(self.frameLeft).grid(row=7,column=3)
        Label(self.frameLeft, text="Délai minimum :    ").grid(row=8,column=3)
        self.sbTempsMin=Spinbox(self.frameLeft, from_ = 1.0, to = 999.8, width=8, increment = 0.1, highlightthickness=2, highlightcolor="light blue1")
        self.sbTempsMin.grid(row=8,column=4)
        self.sbTempsMin.delete(0,END)
        self.sbTempsMin.insert(0,3.0)
        Label(self.frameLeft, text="Délai maximum :    ").grid(row=9,column=3)
        self.sbTempsMax=Spinbox(self.frameLeft, from_ = 1.1, to = 999.9, width=8, increment = 0.1, highlightthickness=2, highlightcolor="light blue1")
        self.sbTempsMax.grid(row=9,column=4)
        self.sbTempsMax.delete(0,END)
        self.sbTempsMax.insert(0,6.0)
        
        #Sélection du statut de la carte
        Label(self.frameLeft).grid(row=10,column=3)
        Label(self.frameLeft, text= "Statut :    ").grid(row=11,column=3)
        optionStatut=[("Publique", "public"), ("Équipe","team"), ("Privée","private")]
        self.Statut = StringVar()
        self.Statut.set("public") #initialisation
        r=11
        for text, status in optionStatut:
            rb=Radiobutton(self.frameLeft, text=text, variable=self.Statut, value=status)
            rb.grid(row=r, column=4, sticky=W)
            r+=1
            
        #Affichage des contrôles et de la légende
        Label(self.frameLeft).grid(row=14,column=2)
        self.frameLegende=Frame(self.frameLeft)
        self.frameLegende.grid(row=15,column=2,columnspan=3)
        Label(self.frameLegende,text="Utilisation de l'éditeur :").pack()
        Label(self.frameLegende,text="\nCliquer dans la grille afin de lui donner\nle focus. Par la suite, utiliser le clavier.\nPlacer les cases de départ des tanks en dernier.\n\nDéplacer le curseur :   Flèche ou WASD\nCycler type de tuile :   Espace\nCycler inversement :   CTRL\nPlacer départ tanks :   Shift", justify = LEFT).pack()
        Label(self.frameLegende,text="\nLégende des couleurs :\n").pack()
        Label(self.frameLegende,text="Plancher",width=15,bg="white",relief="ridge").pack()
        Label(self.frameLegende,text="Mur",width=15,bg="steel blue",relief="ridge").pack()
        Label(self.frameLegende,text="Mur Animé",width=15,bg="green yellow",relief="ridge").pack()
        Label(self.frameLegende,text="Mur Animé Inverse",width=15,bg="olive drab",relief="ridge").pack()
        
    def setupRightFrame(self):
        self.frameCentered=Frame(self.frameRight, highlightthickness=5, highlightcolor="light blue1") #Ce frame sert à centrer verticalement le canvas, peu importe sa taille
        self.frameCentered.pack(expand=1)
        self.grille=Canvas(self.frameCentered,highlightthickness=0,bg="white")
        self.grille.pack()
        self.cursorX=0
        self.cursorY=0
        self.updateSize()
        self.grille.bind("<1>", lambda event: self.grille.focus_set())
        self.grille.bind("<Key>", self.keyPressed)
        
    def keyPressed(self,e):
        if (e.keycode == 39) or (e.keycode ==68): #Keybind Flèche Droite / D
            self.cursorX=self.cursorX+1 if self.cursorX < int(self.sbLargeur.get())-1 else self.cursorX
        elif (e.keycode == 40) or (e.keycode ==83): #Keybind Flèche Bas / S
            self.cursorY=self.cursorY+1 if self.cursorY < int(self.sbHauteur.get())-1 else self.cursorY
        elif (e.keycode == 37) or (e.keycode ==65): #Keybind Flèche Gauche / A
            self.cursorX=self.cursorX-1 if self.cursorX > 0 else self.cursorX
        elif(e.keycode == 38) or (e.keycode ==87): #Keybind Flèche Haut / W
            self.cursorY=self.cursorY-1 if self.cursorY > 0 else self.cursorY
        elif(e.keycode == 32): #Keybind Espace
            self.cycleCell(0)
        elif(e.keycode == 17) or (e.keycode == 223): #Keybind CTRL (ctrl gauche et droite)
            self.cycleCell(1)
        elif(e.keycode == 16): #Keybind Shift
            self.cycleCaseTank()  
        self.updateCursor() #Redessine le curseur
        
    def drawTank(self,tank): #Methode qui sert à afficher un case comme étant un case de départ de tank
        if (tank==1): #Pas le choix de faire if .. else au lieu de just concatener le numero du tank dans le texte, car la concaténation dans les "tags" de canvas tkinter crée des bugs
            self.grille.create_text((self.cursorX*self.cellSize)+(0.5*self.cellSize),(self.cursorY*self.cellSize)+(0.5*self.cellSize), text="Tank 1", tags="tank1")
        else:
            self.grille.create_text((self.cursorX*self.cellSize)+(0.5*self.cellSize),(self.cursorY*self.cellSize)+(0.5*self.cellSize), text="Tank 2", tags="tank2")
        
        
    def cycleCell(self,backward):
        styleCase=self.parent.cycleCell(backward, self.cursorX,self.cursorY) #Demande d'updater la matrice du modele et obtient le type de case à afficher.
        if (styleCase==0):
            color="white"
        elif (styleCase==1):
            color="steel blue"
        elif (styleCase==2):
            color="green yellow"
        else:
            color="olive drab"
        self.grille.create_rectangle(self.cursorX*self.cellSize,self.cursorY*self.cellSize,(self.cursorX+1)*self.cellSize,(self.cursorY+1)*self.cellSize, fill=color)
        if (self.modele.caseTank1==[self.cursorX,self.cursorY]):
            self.grille.delete("tank1")
            self.drawTank(1)
        elif (self.modele.caseTank2==[self.cursorX,self.cursorY]):
            self.grille.delete("tank2")
            self.drawTank(2)
    
        
    def cycleCaseTank(self):
        result = self.parent.cycleCaseTank(self.cursorX,self.cursorY) #Demande de marquer une case en tant que case de départ pour tank
        if (result == 1):
            messagebox.showinfo("Erreur", "Il y a déjà deux cases désigné pour le départ des tanks")
        elif (result == "Tank1"):
            self.drawTank(1)
        elif (result == "Tank2"):
            self.drawTank(2)
        elif (result == "rmTank1"):
            self.grille.delete("tank1")
        else:
            self.grille.delete("tank2")
            
        
    def updateCursor(self):
        self.grille.delete("cursor")
        self.grille.create_rectangle((self.cursorX*self.cellSize)+1.5,(self.cursorY*self.cellSize)+1.5,((self.cursorX+1)*self.cellSize)-1.5,((self.cursorY+1)*self.cellSize)-1.5, outline="blue",width=1.5, tags="cursor")
        self.grille.create_rectangle((self.cursorX*self.cellSize)+3.5,(self.cursorY*self.cellSize)+3.5,((self.cursorX+1)*self.cellSize)-4.5,((self.cursorY+1)*self.cellSize)-4.5, outline="light blue",width=0.5, tags="cursor")
                
    def updateSize(self): #Appeler à chaque fois que l'on redimentionne la grille avec les spinbox
        nbCaseLargeur=int(self.sbLargeur.get())
        largeur=nbCaseLargeur*self.cellSize
        self.grille.config(width=largeur+1)
        self.grille.delete("width")
        nbCaseHauteur=int(self.sbHauteur.get())
        hauteur=nbCaseHauteur*self.cellSize
        self.grille.config(height=hauteur+1)
        self.grille.delete("height")
        for i in range (0,nbCaseLargeur+1):
            c=i*self.cellSize
            self.grille.create_line(c,0,c,hauteur, tags="width")
        for i in range (0,nbCaseHauteur+1):
            c=i*self.cellSize
            self.grille.create_line(0,c,largeur,c, tags="height")
        self.cursorX=0
        self.cursorY=0
        self.grille.delete("tank1","tank2") #On enleve les cases de départ de tank pour éviter des bug.
        self.parent.removeCaseTank()        #Par exemple: on marque la 12e case comme case de départ, puis on redimentionne a une largeur de 11 case...
        self.updateCursor()
        
    def resetEditor(self):
        self.parent.resetEditor()
        self.frameRight.destroy()
        self.frameLeft.destroy()
        self.createFrames()
        self.setupLeftFrame()
        self.setupRightFrame()
    
    def sauvegarder(self):
        self.parent.sauvegarder(self.entryTitre.get(),int(self.sbHauteur.get()),int(self.sbLargeur.get()),float(self.sbTempsMin.get()),float(self.sbTempsMax.get()),self.Statut.get())
        
    def afficherAide(self):
        startfile("lisez-moi.txt")

    def afficherContrainte(self):
        messagebox.showinfo("Contraintes de niveaux", "-> Les niveaux doivent avoir un noms unique\n-> La largeur et hauteur doivent être compris entre 6 et 12 cases\n-> Le délai minimum doit être plus petit que le délai maximum\n-> Deux cases doivent être désignées pour la position initiale des tanks\n\n\nL'éditeur de niveau enverra un message d'information lors de la sauvegarde si l'une de ces contraintes n'est pas respectées.")
        
    def afficherErreur(self,e):
        messagebox.showinfo("Erreur",e)
    
############################# MODELE #############################
        
class Modele():
    def __init__(self,parent):
        self.parent=parent
        self.matriceNiveau=[[0 for x in range(12)] for x in range(12)] #Initialise une matrice de 12 par 12 avec 0 dans chaque case.
        self.caseTank1=[]
        self.caseTank2=[]
        
    def login(self,username):
    	self.username=username
        
    def cycleCell(self,backward,posX,posY):
        if (backward):
            self.matriceNiveau[posY][posX]-=1
        else:
            self.matriceNiveau[posY][posX]+=1
        if (self.matriceNiveau[posY][posX] > 3):
            self.matriceNiveau[posY][posX]=0
        elif (self.matriceNiveau[posY][posX] < 0):
            self.matriceNiveau[posY][posX]=3
        return self.matriceNiveau[posY][posX]
    
    def cycleCaseTank(self, posX, posY):
        if (self.caseTank1==[posX, posY]):
            self.caseTank1=[]
            return "rmTank1"
        elif (self.caseTank2==[posX, posY]):
            self.caseTank2=[]
            return "rmTank2"
        elif (self.caseTank1==[]):
            self.caseTank1.append(posX)
            self.caseTank1.append(posY)
            return "Tank1"
        elif (self.caseTank2==[]):
            self.caseTank2.append(posX)
            self.caseTank2.append(posY)
            return "Tank2"
        else:
            return 1
        
    def removeCaseTank(self):
        self.caseTank1=[]
        self.caseTank2=[]
            
    
    def resetEditor(self):
        self.matriceNiveau=[[0 for x in range(12)] for x in range(12)] #Initialise une matrice de 12 par 12 avec 0 dans chaque case.
        self.removeCaseTank()
        
    def sauvegarder(self, titre, hauteur, largeur, delaiMin, delaiMax, statut):
        if (titre == ""):
            self.parent.afficherErreur("Veuillez entrer un titre")
        elif (delaiMin >= delaiMax):
            self.parent.afficherErreur("Le délai maximum doit être plus grand que le délai minimum.")
        elif (self.caseTank1==[]) or (self.caseTank2==[]):
            self.parent.afficherErreur("Vous devez définir les case de départs des deux tanks")
        else:
            count=0
            for i in self.matriceNiveau:
                for j in i:
                    if (count >= hauteur):
                        j=None
                    elif (j >= largeur):
                        j=None
                    count += 1
            dictNiveau={'nomNiveau': titre,'statutNiveau': statut, 'delaiApparitionMinimum':delaiMin, 'delaiApparitionMaximum':delaiMax, 'tank1PosX': self.caseTank1[0],'tank1PosY': self.caseTank1[1],'tank2PosX':self.caseTank2[0],'tank2PosY':self.caseTank2[1]}
            try:
                self.dto=DTONiveau.DTONiveau()
                self.dto.setNiveau(dictNiveau) ##BUG TO FIX Here when passing dictionnary to DTONiveau
                self.dto.setMatriceNiveau(self.matriceNiveau)
                self.dao=DAOOracleNiveau.DAOOracleNiveau()
                erreur=0
                erreur=self.dao.ecrireNiveau(self.dto)
                if (erreur):
                    self.parent.afficherErreur("Échec de la connection à la base de donnée")
                else:
                    self.dao.ecrireMatrice(self.dto,titre)
            except:
                self.parent.afficherErreur("Échec de la sauvegarde du niveau")
            else:
                self.parent.vue.resetEditor()
                messagebox.showinfo("Sauvegarder","La sauvegarde a été réussie")               
                
            
                
                    
    
        
    
    
########################### CONTROLEUR ###########################

class Controleur():
    def __init__(self):
        self.modele=Modele(self)
        self.vue=Vue(self, self.modele)
        
    def cycleCell(self,backward, posX, posY):
        styleCase=self.modele.cycleCell(backward, posX, posY)
        return styleCase
    
    def cycleCaseTank(self, posX, posY):
        erreur=self.modele.cycleCaseTank(posX, posY)
        return erreur
    
    def removeCaseTank(self):
        self.modele.removeCaseTank()
    
    def resetEditor(self):
        self.modele.resetEditor()
        
    def sauvegarder(self, titre, hauteur, largeur, delaiMin, delaiMax, statut):
        result=self.modele.sauvegarder(titre, hauteur, largeur, delaiMin, delaiMax, statut)
        return result
    
    def login(self,username):
    	self.modele.login(username)
    
    def afficherErreur(self,e):
        self.vue.afficherErreur(e)
        

if __name__ == '__main__':
    c = Controleur()
    c.vue.tk.mainloop()